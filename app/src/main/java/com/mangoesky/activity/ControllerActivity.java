package com.mangoesky.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mangoesky.R;
import com.mangoesky.apiendpoint.ApiEndPoint;
import com.mangoesky.apiresponsewrapper.CCResponse;
import com.mangoesky.apiresponsewrapper.DetailTHResponse;
import com.mangoesky.apiresponsewrapper.HistoryResponse;
import com.mangoesky.apiresponsewrapper.LoginResponse;
import com.mangoesky.apiresponsewrapper.ProfileResponse;
import com.mangoesky.apiresponsewrapper.ServiceResponse;
import com.mangoesky.apiresponsewrapper.TopupHistoryResponse;
import com.mangoesky.apiresponsewrapper.TopupPacketResponse;
import com.mangoesky.fragment.CareCenterFragment;
import com.mangoesky.fragment.ChangePasswordFragment;
import com.mangoesky.fragment.EditProfileFragment;
import com.mangoesky.fragment.HistoryContentFragment;
import com.mangoesky.fragment.HistoryFragment;
import com.mangoesky.fragment.NotificationFragment;
import com.mangoesky.fragment.ProfileFragment;
import com.mangoesky.fragment.ServiceFragment;
import com.mangoesky.fragment.TopupHisotyFragment;
import com.mangoesky.fragment.TopupInvoiceFragment;
import com.mangoesky.fragment.TopupListFragment;
import com.mangoesky.fragment.VoucherFragment;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.SimpleModel;
import com.mangoesky.model.TopupHistory;
import com.mangoesky.model.User;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Ramdan Firdaus on 1/2/2017.
 */
public class ControllerActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener,
        HistoryFragment.HistoryFragmentIntf,
        HistoryContentFragment.HistoryContentInf,
        ProfileFragment.ProfileFragmentInf,
        EditProfileFragment.EditProfileInf,
        ChangePasswordFragment.ChangePasswordFragmentInf,
        VoucherFragment.VoucherFragmentInf,
        ServiceFragment.ServiceFragmentInf,
        TopupListFragment.TopupListFragmentInf,
        TopupHisotyFragment.TopupHisotryFragmentInf,
        TopupInvoiceFragment.TopupInvoiceFragmentInf,
        NotificationFragment.NotificationFragmentInf,
        CareCenterFragment.CareCenterClickListener,
        DrawerLayout.DrawerListener{

    private AppBarLayout appBarLayout;
    private Toolbar toolbar;
    private ProfileResponse profileResponse;
    private ProgressDialog progressDialog;
    private boolean isOk;
    private final String LOG_TAG = ControllerActivity.class.getSimpleName();
    private TextView tvCustId;
    private TextView tvCustFullname;
    private String custId;
    private String custPass;
    private Retrofit retrofitClient;
    private Context mContext;
    private User userFromApi;
    private LoginResponse loginResponse;
    private TopupHistoryResponse topupHistoryResponse;

    private ServiceResponse mServiceResponse = null;
    private TopupPacketResponse mTopupPacketResponse = null;
    private CCResponse mCCResponse = null;
    private DrawerLayout mDrawer;
    private int mSelectedDrawer;
    private boolean clickNavigationDrawerFlag = false;

    NavigationView navigationView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);

        /*------ View Initialitation -------*/
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        mDrawer.addDrawerListener(this);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);

        disableNavigationViewScrollbars(navigationView);

        // set checked on service nav
        navigationView.setCheckedItem(R.id.item_nav_service);

        // get textview from navigation header
        View header = navigationView.getHeaderView(0);
        tvCustFullname = (TextView) header.findViewById(R.id.nav_tv_cust_name);
        tvCustId = (TextView) header.findViewById(R.id.nav_tv_cust_id);

        // this used for every request to API
        custId = Utility.getCustIdPrf(this);
        custPass = Utility.getCustPassPrf(this);

        // retrofit client for DRY (Dont repeat your selft)
        retrofitClient = Utility.getClient(custId, custPass);

        // Activity context
        mContext = ControllerActivity.this;

        /*------ End View Initialitation -------*/


        Intent i = getIntent();

        // Set fullname and custid in navbar header
        // This condition will true when intent come from
        // splash activity
        if (i.hasExtra(AppConst.PARC_LOGIN_RESP)) {

            loginResponse = i.getParcelableExtra(AppConst.PARC_LOGIN_RESP);
            if (loginResponse != null) {
                userFromApi = loginResponse.getUser();
                tvCustFullname.setText(userFromApi.getFullname());
                tvCustId.setText(Utility.getCustIdPrf(ControllerActivity.this));
            }
        }

        // intent come from PaymentConfirmActivity
        if (i.hasExtra(AppConst.TAG_FLAG)) {

            if (i.getStringExtra(AppConst.TAG_FLAG)
                    .equals(PaymentConfirmActivity.class.getSimpleName())){
                navigationView.setCheckedItem(R.id.item_nav_topuphistory);
                getTopupHistoryFromApi();
            }

        } else {
            // Redirect to service info screen on first load
            getServiceFromApi(false);
        }



    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int menuItemId = menuItem.getItemId();

        clickNavigationDrawerFlag = true;

        switch (menuItemId) {
            case R.id.item_nav_service:
                mSelectedDrawer = R.id.item_nav_service;
                break;
            case R.id.item_nav_history:
                mSelectedDrawer = R.id.item_nav_history;
                break;
            case R.id.item_nav_topup:
                mSelectedDrawer = R.id.item_nav_topup;
                break;
            case R.id.item_nav_profile:
                mSelectedDrawer = R.id.item_nav_profile;
                break;
            case R.id.item_nav_notifcations:
                mSelectedDrawer = R.id.item_nav_notifcations;
                break;
            case R.id.item_nav_voucher:
                mSelectedDrawer = R.id.item_nav_voucher;
                break;
            case R.id.item_nav_topuphistory:
                mSelectedDrawer = R.id.item_nav_topuphistory;
                break;
            case R.id.item_nav_carecenter:
                mSelectedDrawer = R.id.item_nav_carecenter;
                break;
            case R.id.item_nav_logout:
                mSelectedDrawer = R.id.item_nav_logout;
                break;

        }

        mDrawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Utility.emptCustFullnamePrf(mContext);
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onClickSearchHistory(int month, int year, final ProgressBar historyPbar,
                                     final String selectedDate, Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);
        switch (itemId) {
            case R.id.humb_history:
                showNavigationDrawerFromLeft();
                break;
            case R.id.btn_search_history:
                progressDialog.show();
                ApiEndPoint endPoint = Utility.getClient(custId, custPass)
                        .create(ApiEndPoint.class);
                Call<HistoryResponse> call = endPoint.getHistory(month, year);
                call.enqueue(new Callback<HistoryResponse>() {
                    @Override
                    public void onResponse(Call<HistoryResponse> call, Response<HistoryResponse> response) {
                        progressDialog.dismiss();
                        HistoryResponse historyResponse = response.body();
                        if (historyResponse != null) {

                            Bundle args = new Bundle();
                            args.putParcelable(AppConst.PARC_USER, userFromApi);
                            args.putParcelable(AppConst.PARC_HISTORY_RESP, historyResponse);
                            args.putString(AppConst.SELECTED_DATE, selectedDate);
                            args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.history_title));

                            HistoryContentFragment hcf = new HistoryContentFragment();
                            hcf.setArguments(args);

                            setUpFragment(hcf);

                        } else {
                            if (response.code() == AppConst.UNAUTHORIZED) {
                                Utility.doReLogin(ControllerActivity.this);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<HistoryResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                        if (t instanceof  IOException) {
                            Utility.displayAlert(ControllerActivity.this, getString(R.string.msg_no_internet));
                        }
                    }
                });
                break;
        }

    }

    @Override
    public void historyContentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);

        switch (itemId) {
            case R.id.new_btn_back:
                getSupportFragmentManager().popBackStack();
                break;
            case R.id.humb_content_history:
                showNavigationDrawerFromLeft();
                break;
        }

    }

    @Override
    public void onItemClickListener(int itemId) {
        switch (itemId) {
            case R.id.btn_go_editprofile:
                EditProfileFragment e = new EditProfileFragment();
                if (userFromApi != null) {
                    Bundle args = new Bundle();
                    args.putParcelable(AppConst.PARC_USER, userFromApi);
                    e.setArguments(args);
                }
                setUpFragment(e);
                break;
            case R.id.btn_go_editpass:
                setUpFragment(new ChangePasswordFragment());
                break;
            case R.id.humb_view_profile:
                showNavigationDrawerFromLeft();
                break;
            default:
                Log.d(LOG_TAG, "undefined id.");
        }
    }

    @Override
    public void EditProfileClickListener(int itemId, Bundle args) {
        switch (itemId) {
            case R.id.btn_edit_back:
                getSupportFragmentManager().popBackStack();
                break;
            case R.id.btn_edit_simpan:
                User u = args.getParcelable(AppConst.PARC_USER);
                editProfileWithApi(u);
                break;
            default:
                Log.d(LOG_TAG, "undefined id.");
        }
    }

    @Override
    public void ChangePasswordClickListerner(int itemId, Bundle args) {
        switch (itemId) {
            case R.id.btn_cp_back:
                getSupportFragmentManager().popBackStack();
                break;
            case R.id.btn_cp_simpan:
                changePasswordWithApi(args);
                break;
            default:
                Log.d(LOG_TAG, "undefined itemId.");
                break;
        }
    }

    @Override
    public void VoucherFragmentOnClickListener(int itemId, Bundle args, TextView tvVoucherCode) {
        switch (itemId) {
            case R.id.voucher_btn_submit:
                submitVoucherWithApi(args, tvVoucherCode);
                break;
            case R.id.humb_voucher:
                showNavigationDrawerFromLeft();
                break;
            default:
                Log.d(LOG_TAG, "undefined id.");
                break;
        }
    }

    @Override
    public void onSwipeListener() {
        getServiceFromApi(true);
    }

    @Override
    public void serviceFragmentClicListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);
        switch (itemId) {
            case R.id.humb_service:
                mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public void topupListFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);

        switch (itemId) {
            case AppConst.FINPAY_ACTION_TAG:
                if (userFromApi != null) {
                    Utility.setCustFullnamePrf(mContext, userFromApi.getFullname());

                    args.putString(AppConst.TAG_FULLNAME, userFromApi.getFullname());

                    Intent i = new Intent(mContext, PaymentConfirmActivity.class);
                    i.putExtra(AppConst.INTENT_EX_PACKET, args);


                    startActivityForResult(i, AppConst.FINPAY_ACTIVITY_REQUEST);
                }
                break;
            case R.id.humb_topup_list:
                showNavigationDrawerFromLeft();
                break;
        }




    }

    @Override
    public void topupHistoryFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);
        switch (itemId) {
            case AppConst.SEE_INVOICE_ACTION_TAG:
                progressDialog.show();
                TopupHistory topup = args.getParcelable(AppConst.PARC_PACKET_TOPUPHISTORY);
                ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
                Call<DetailTHResponse> call = endPoint.getDetailTH("topup_order_detail/"+topup.getId());
                call.enqueue(new Callback<DetailTHResponse>() {
                    @Override
                    public void onResponse(Call<DetailTHResponse> call, Response<DetailTHResponse> response) {
                        progressDialog.dismiss();
                        DetailTHResponse result = response.body();
                        if (result != null) {
                            if (result.isSuccess()) {
                                TopupHistory payment = result.getTopupHistory();
                                String expDate = "";
                                String expTime = "";
                                try {
                                    String unFormatExp = payment.getExpiration();
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                                    Date newDate = format.parse(unFormatExp);

                                    format = new SimpleDateFormat("dd MMMM yyy", Locale.US);
                                    expDate = format.format(newDate);

                                    format = new SimpleDateFormat("hh:mm:ss", Locale.US);
                                    expTime = format.format(newDate);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Bundle args = new Bundle();
                                args.putString(AppConst.TAG_INVOICE, payment.getInvoice());
                                args.putString(AppConst.TAG_NAME, payment.getTopupPacketName());
                                args.putString(AppConst.TAG_PRICE, payment.getPrice());
                                args.putString(AppConst.TAG_CODE, payment.getCode());
                                args.putString(AppConst.TAG_DATE, expDate);
                                args.putString(AppConst.TAG_TIME, expTime);
                                args.putString(AppConst.TAG_FLAG, "ControllerActivity");

                                TopupInvoiceFragment f = new TopupInvoiceFragment();
                                f.setArguments(args);
                                setUpFragment(f);

                            } else {
                                Utility.displayAlert(mContext, result.getMsg());
                            }

                        } else {
                            if (response.code() == AppConst.UNAUTHORIZED) {
                                Utility.doReLogin(mContext);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DetailTHResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                        if (t instanceof IOException) {
                            Utility.displayNoIntAlert(mContext);
                        }
                    }
                });
                break;
            case R.id.humb_topup_history:
                showNavigationDrawerFromLeft();
                break;
        }

    }

    @Override
    public void topupInvoiceClickListener(Bundle args) {
        if (args != null) {
            int viewId = args.getInt(AppConst.TOPUPINV_CLICK_ID);
            switch (viewId) {
                case R.id.btn_finish_order:
                    super.onBackPressed();
                    break;
                case R.id.copy_pc:
                    String paymentCode = args.getString(AppConst.TAG_CODE);
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText("text", paymentCode);
                    clipboard.setPrimaryClip(clipData);
                    Toast.makeText(mContext, getString(R.string.copied_code_text), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.btn_how_topay:
                    startActivity(new Intent(mContext, TopupPaymentManualActivity.class));
                    break;
                case R.id.btn_list_topup:
                    super.onBackPressed();
                    break;
                default:
                    Log.d(LOG_TAG, "undefined viewId");
                    break;
            }
        }
    }

    @Override
    public void NotificationFragmentClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);
        switch (itemId) {
            case R.id.humb_notification:
                showNavigationDrawerFromLeft();
                break;
        }
    }

    @Override
    public void CareCenterClickListener(Bundle args) {
        int itemId = args.getInt(AppConst.ITEM_ID);

        switch (itemId) {
            case R.id.humb_care_center:
                showNavigationDrawerFromLeft();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConst.FINPAY_ACTIVITY_REQUEST) {
            if (resultCode == AppConst.FINPAY_RESULT_SEE_TOPUP_HISTORY) {
                navigationView.setCheckedItem(R.id.item_nav_topuphistory);
                getTopupHistoryFromApi();
            }
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {}

    @Override
    public void onDrawerOpened(View drawerView) { }

    @Override
    public void onDrawerStateChanged(int newState) {}

    @Override
    public void onDrawerClosed(View drawerView) {

        Bundle args = new Bundle();

        if (clickNavigationDrawerFlag) {
            switch (mSelectedDrawer) {
                case R.id.item_nav_service:
                    getServiceFromApi(false);
                    break;
                case R.id.item_nav_history:
                    HistoryFragment historyFragment = new HistoryFragment();
                    args.putParcelable(AppConst.PARC_USER, userFromApi);
                    args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.history_title));
                    historyFragment.setArguments(args);
                    setUpFragment(historyFragment);
                    break;
                case R.id.item_nav_topup:
                    getToupPacketFromApi();
                    break;
                case R.id.item_nav_profile:
                    ProfileFragment profileFragment = new ProfileFragment();
                    if (userFromApi != null) {
                        args = new Bundle();
                        args.putParcelable(AppConst.PARC_USER, userFromApi);
                        profileFragment.setArguments(args);
                    }
                    setUpFragment(profileFragment);
                    break;
                case R.id.item_nav_notifcations:
                    args.putParcelable(AppConst.PARC_USER, userFromApi);
                    args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.notif));
                    NotificationFragment notificationFragment = new NotificationFragment();
                    notificationFragment.setArguments(args);
                    setUpFragment(notificationFragment);
                    break;
                case R.id.item_nav_voucher:
                    setUpFragment(new VoucherFragment());
                    break;
                case R.id.item_nav_topuphistory:
                    getTopupHistoryFromApi();
                    break;
                case R.id.item_nav_carecenter:
                    getCCFromApi();
                    break;
                case R.id.item_nav_logout:
                    logoutWithApi();
                    break;
            }

            clickNavigationDrawerFlag = false;
        }

    }

    private void setUpFragment(Fragment f) {
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_container, f);
        String tag = fragmentTransaction.toString();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void getToupPacketFromApi() {

        if (mTopupPacketResponse == null) {
            progressDialog.show();
            ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
            Call<TopupPacketResponse> call = endPoint.getListTopupPacket();
            call.enqueue(new Callback<TopupPacketResponse>() {
                @Override
                public void onResponse(Call<TopupPacketResponse> call, Response<TopupPacketResponse> response) {
                    progressDialog.dismiss();
                    mTopupPacketResponse = response.body();
                    if (mTopupPacketResponse != null) {

                        Bundle args = new Bundle();
                        args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.topup_packet));
                        args.putParcelable(AppConst.PARC_USER, userFromApi);
                        args.putParcelable(AppConst.PARC_TOPUP_PACKET_RESP, mTopupPacketResponse);
                        TopupListFragment f = new TopupListFragment();
                        f.setArguments(args);
                        setUpFragment(f);

                    } else {
                        if (response.code() == AppConst.UNAUTHORIZED) {
                            Utility.doReLogin(mContext);
                        }
                    }
                }

                @Override
                public void onFailure(Call<TopupPacketResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    if (t instanceof IOException) {
                        Utility.displayAlert(mContext, getString(R.string.msg_no_internet));
                    }
                }
            });

        } else {
            Bundle args = new Bundle();
            args.putParcelable(AppConst.PARC_USER, userFromApi);
            args.putParcelable(AppConst.PARC_TOPUP_PACKET_RESP, mTopupPacketResponse);
            args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.topup_packet));
            TopupListFragment f = new TopupListFragment();
            f.setArguments(args);
            setUpFragment(f);
        }

    }

    private void editProfileWithApi(User u) {
        //progressDialog.setMessage();
        progressDialog.show();
        userFromApi = u; // update wih new user data
        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<SimpleModel> call = endPoint.updateProfile(
                u.getFullname(),
                u.getEmail(),
                u.getAddress(),
                u.getPhone(),
                u.getGender()
        );
        call.enqueue(new Callback<SimpleModel>() {
            @Override
            public void onResponse(Call<SimpleModel> call, Response<SimpleModel> response) {

                SimpleModel result = response.body();
                if (result != null) {
                    tvCustFullname.setText(userFromApi.getFullname());

                    getSupportFragmentManager().popBackStack();

                    ProfileFragment profileFragment = new ProfileFragment();
                    if (userFromApi != null) {
                        Bundle args = new Bundle();
                        args.putParcelable(AppConst.PARC_USER, userFromApi);
                        profileFragment.setArguments(args);
                    }
                    setUpFragment(profileFragment);
                    progressDialog.dismiss();
                    Toast.makeText(mContext, getString(R.string.profile_updated),
                            Toast.LENGTH_SHORT).show();

                } else {
                    if (response.code() == AppConst.UNAUTHORIZED) {
                        Utility.doReLogin(mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayAlert(mContext, getString(R.string.msg_no_internet));
                }
            }
        });
    }

    private void changePasswordWithApi(Bundle args) {
        progressDialog.show();
        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<SimpleModel> call = endPoint.changePasswordApi(
                args.getString(AppConst.OLD_PASSWORD),
                args.getString(AppConst.NEW_PASSWORD),
                args.getString(AppConst.CONFIRM_NEW_PASSWORD)
        );
        call.enqueue(new Callback<SimpleModel>() {
            @Override
            public void onResponse(Call<SimpleModel> call, Response<SimpleModel> response) {
                progressDialog.dismiss();
                SimpleModel m = response.body();
                if (m != null) {
                    if (m.isSuccess()) {
                        Utility.emptyCustPrf(mContext);

                        Toast.makeText(mContext, getString(R.string.password_edited), Toast.LENGTH_SHORT)
                                .show();
                        startActivity(new Intent(mContext, LoginActivity.class));
                        finish();

                    } else {
                        Utility.displayAlert(mContext, m.getMsg());
                    }


                } else if (response.code() == AppConst.UNAUTHORIZED) {
                    Utility.doReLogin(mContext);
                }
            }

            @Override
            public void onFailure(Call<SimpleModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayAlert(mContext, getString(R.string.msg_no_internet));
                }
            }
        });
    }

    private void logoutWithApi() {
        progressDialog.show();
        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<SimpleModel> call = endPoint.logoutApi(Utility.getCustRegId(mContext));
        call.enqueue(new Callback<SimpleModel>() {
            @Override
            public void onResponse(Call<SimpleModel> call, Response<SimpleModel> response) {
                progressDialog.dismiss();
                SimpleModel m = response.body();
                if (m != null) {

                    Utility.emptyCustPrf(mContext);
                    startActivity(new Intent(mContext, IntroslideActivity.class));
                    finish();

                } else if (response.code() == AppConst.UNAUTHORIZED){
                    Utility.doReLogin(mContext);
                }
            }

            @Override
            public void onFailure(Call<SimpleModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayAlert(mContext, getString(R.string.msg_no_internet));
                }
            }
        });
    }

    private void getCCFromApi() {

        if (mCCResponse == null) {
            progressDialog.show();
            ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
            Call<CCResponse> call = endPoint.getCCApi();
            call.enqueue(new Callback<CCResponse>() {
                @Override
                public void onResponse(Call<CCResponse> call, Response<CCResponse> response) {
                    progressDialog.dismiss();
                    mCCResponse = response.body();
                    if (mCCResponse != null) {

                        Bundle args = new Bundle();
                        args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.title_care_center));
                        args.putParcelable(AppConst.PARC_USER, userFromApi);
                        args.putParcelable(AppConst.PARC_CC_RESP, mCCResponse);
                        CareCenterFragment c = new CareCenterFragment();
                        c.setArguments(args);
                        setUpFragment(c);

                    } else if (response.code() == AppConst.UNAUTHORIZED){
                        Utility.doReLogin(mContext);
                    }
                }

                @Override
                public void onFailure(Call<CCResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    if (t instanceof IOException) {
                        Utility.displayNoIntAlert(mContext);
                    }
                }
            });

        } else {
            Bundle args = new Bundle();
            args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.title_care_center));
            args.putParcelable(AppConst.PARC_USER, userFromApi);
            args.putParcelable(AppConst.PARC_CC_RESP, mCCResponse);
            CareCenterFragment c = new CareCenterFragment();
            c.setArguments(args);
            setUpFragment(c);
        }
    }

    private void submitVoucherWithApi(Bundle args, final TextView tvVoucherCode) {
        progressDialog.show();
        String voucherCode = args.getString(AppConst.POST_VOUCHER_CODE);

        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<SimpleModel> call = endPoint.submitVoucherApi(voucherCode);
        call.enqueue(new Callback<SimpleModel>() {
            @Override
            public void onResponse(Call<SimpleModel> call, Response<SimpleModel> response) {
                progressDialog.dismiss();
                SimpleModel result = response.body();
                if (result != null) {

                    String serverMsg = result.getMsg();
                    if (result.isSuccess()) tvVoucherCode.setText("");

                    Utility.displayAlert(mContext, serverMsg);

                } else if (response.code() == AppConst.UNAUTHORIZED){
                    Utility.doReLogin(mContext);
                }
            }

            @Override
            public void onFailure(Call<SimpleModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContext);
                }
            }
        });
    }

    private void getServiceFromApi(final boolean fromSwipe) {

        progressDialog.show();
        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<ServiceResponse> call = endPoint.getServiceApi();
        call.enqueue(new Callback<ServiceResponse>() {
            @Override
            public void onResponse(Call<ServiceResponse> call, Response<ServiceResponse> response) {
                progressDialog.dismiss();
                mServiceResponse = response.body();
                if (mServiceResponse != null) {

                    if (fromSwipe && mServiceResponse.getServiceModel().isSuccess())
                        Toast.makeText(mContext, getString(R.string.data_updated), Toast.LENGTH_SHORT).show();

                    Bundle args = new Bundle();
                    args.putParcelable(AppConst.PARC_USER, userFromApi);
                    args.putParcelable(AppConst.PARC_SERVICE_RESP, mServiceResponse);
                    args.putString(AppConst.TAG_TITLE_USER_INFO,
                            Utility.getGreetingText(mContext) + ",");

                    ServiceFragment s = new ServiceFragment();
                    s.setArguments(args);
                    setUpFragment(s);

                } else if (response.code() == AppConst.UNAUTHORIZED){
                    Utility.doReLogin(mContext);
                }
            }

            @Override
            public void onFailure(Call<ServiceResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContext);
                }
            }
        });

    }

    private void getTopupHistoryFromApi(){
        progressDialog.show();
        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<TopupHistoryResponse> call = endPoint.getTopupHistory();
        call.enqueue(new Callback<TopupHistoryResponse>() {
            @Override
            public void onResponse(Call<TopupHistoryResponse> call, Response<TopupHistoryResponse> response) {
                progressDialog.dismiss();
                topupHistoryResponse = response.body();
                if (topupHistoryResponse != null) {

                    Bundle args = new Bundle();
                    args.putParcelable(AppConst.PARC_USER, userFromApi);
                    args.putString(AppConst.TAG_TITLE_USER_INFO, getString(R.string.topup_history_text));
                    args.putParcelable(AppConst.PARC_PACKET_TOPUPHISTORY, topupHistoryResponse);

                    TopupHisotyFragment f = new TopupHisotyFragment();
                    f.setArguments(args);
                    setUpFragment(f);

                } else {
                    if (response.code() == AppConst.UNAUTHORIZED) {
                        Utility.doReLogin(mContext);
                    }
                }
            }

            @Override
            public void onFailure(Call<TopupHistoryResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(mContext);
                }
            }
        });
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    private void showNavigationDrawerFromLeft() {
        mDrawer.openDrawer(Gravity.LEFT);
    }


}
