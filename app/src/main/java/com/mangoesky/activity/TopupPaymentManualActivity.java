package com.mangoesky.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TopupPaymentManualActivity extends AppCompatActivity {

    WebView view;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup_payment_manual);

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait..");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(getString(R.string.paymen_manual_text));

        url = AppConst.URL_PAYMENT_MANUAL_FINNET;
        view = (WebView) this.findViewById(R.id.webView);
        view.getSettings().setJavaScriptEnabled(true);
        view.setVisibility(View.GONE);
        view.setWebViewClient(new MyBrowser(progressDialog));
        view.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        private ProgressDialog progressDialog;

        public MyBrowser(ProgressDialog progressDialog){
            this.progressDialog = progressDialog;
            progressDialog.show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
            view.setVisibility(View.VISIBLE);
            super.onPageFinished(view, url);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
