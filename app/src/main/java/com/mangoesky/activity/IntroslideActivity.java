package com.mangoesky.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.apiendpoint.ApiEndPoint;
import com.mangoesky.apiresponsewrapper.IntroSlideResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.IntroSlide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
// http://www.mocky.io/v2/58f97724110000d909d20ec5
public class IntroslideActivity extends AppCompatActivity {

    private MainPagerAdapter pagerAdapter;
    private ViewPager sliderViewPager;

    private ProgressDialog progressDialog;
    private Retrofit mRetrofitClient;
    private List<IntroSlide> mIntroSlideList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introslide);

        ButterKnife.bind(this);

        // making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        pagerAdapter = new MainPagerAdapter();
        sliderViewPager = (ViewPager) findViewById(R.id.slider_view_pager);
        sliderViewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout_intro);
        tabLayout.setupWithViewPager(sliderViewPager, true);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.please_wait));

        mRetrofitClient = Utility.getDummyClient("", "");

        /*------ End of View initialitation -------*/

        getIntroSlideDataFromApi();
    }

    @OnClick(R.id.btn_go_login) public void goLogin() {
        startActivity(new Intent(IntroslideActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MainPagerAdapter extends PagerAdapter {

        // This holds all the currently displayable views, in order from left to right.
        private ArrayList<View> views = new ArrayList<View>();

        //-----------------------------------------------------------------------------
        // Used by ViewPager.  "Object" represents the page; tell the ViewPager where the
        // page should be displayed, from left-to-right.  If the page no longer exists,
        // return POSITION_NONE.
        @Override
        public int getItemPosition(Object object) {
            int index = views.indexOf(object);
            if (index == 1)
                return POSITION_NONE;
            else
                return index;
        }

        //-----------------------------------------------------------------------------
        // Used by ViewPager.  Called when ViewPager needs a page to display; it is our job
        // to add the page to the container, which is normally the ViewPager itself.  Since
        // all our pages are persistent, we simply retrieve it from our "views" ArrayList.
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v = views.get(position);
            container.addView(v);
            return v;
        }

        //-----------------------------------------------------------------------------
        // Used by ViewPager.  Called when ViewPager no longer needs a page to display; it
        // is our job to remove the page from the container, which is normally the
        // ViewPager itself.  Since all our pages are persistent, we do nothing to the
        // contents of our "views" ArrayList.
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(views.get(position));
        }

        //-----------------------------------------------------------------------------
        // Used by ViewPager; can be used by app as well.
        // Returns the total number of pages that the ViewPage can display.  This must
        // never be 0.
        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        //-----------------------------------------------------------------------------
        // Add "view" to right end of "views".
        // Returns the position of the new view.
        // The app should call this to add pages; not used by ViewPager.
        public int addView(View view, int postion) {
            views.add(view);
            return postion;
        }
    }

    private void getIntroSlideDataFromApi() {
        progressDialog.show();
        ApiEndPoint endPoint = mRetrofitClient.create(ApiEndPoint.class);
        Call<IntroSlideResponse> call = endPoint.getIntroSlides();
        call.enqueue(new retrofit2.Callback<IntroSlideResponse>() {
            @Override
            public void onResponse(Call<IntroSlideResponse> call, Response<IntroSlideResponse> response) {
                progressDialog.dismiss();
                if (response.body() != null) {

                    if (response.body().getSuccess()) {
                        mIntroSlideList = response.body().getIntroSlides();

                        for (int i = 0; i < mIntroSlideList.size(); i++) {
                            LayoutInflater inflater = getLayoutInflater();

                            FrameLayout v0 = (FrameLayout) inflater.inflate(R.layout.slider_item, null);
                            final ImageView imgIntroSlider = (ImageView) v0.findViewById(R.id.img_intro_slider);
                            final ProgressBar introProgress = (ProgressBar) v0.findViewById(R.id.intro_img_progress);
                            TextView introTextTitle = (TextView) v0.findViewById(R.id.intro_text_title);
                            TextView introTextContent = (TextView) v0.findViewById(R.id.intro_text_content);

                            IntroSlide introSlide = mIntroSlideList.get(i);

                            introTextTitle.setText(introSlide.getTitle());
                            introTextContent.setText(introSlide.getText());

                            introProgress.setVisibility(View.VISIBLE);
                            Picasso
                                    .with(IntroslideActivity.this)
                                    .load(introSlide.getImage())
                                    .fit()
                                    .into(imgIntroSlider, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            introProgress.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                            pagerAdapter.addView(v0, i);
                            pagerAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<IntroSlideResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayNoIntAlert(IntroslideActivity.this);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
