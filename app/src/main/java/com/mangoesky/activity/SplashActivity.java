package com.mangoesky.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Space;

import com.mangoesky.R;
import com.mangoesky.apiendpoint.ApiEndPoint;
import com.mangoesky.apiresponsewrapper.LoginResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.SimpleModel;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends BaseActivity {
    private final String LOG_TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        checkInternet();
    }

    private void checkInternet() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            doChecking();

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(AppConst.NO_CONNECTION_MSG)
                    .setTitle("Mangoesky")
                    .setCancelable(false)
                    .setNegativeButton("Batal",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {

                                    dialog.cancel();
                                    System.exit(0);
                                }
                            })
                    .setPositiveButton("Coba kembali",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {

                                    dialog.cancel();
                                    checkInternet();
                                }
                            })

                    .show();
        }
    }

    public void doChecking(){

        String customer_id = Utility.getCustIdPrf(this);
        String password = Utility.getCustPassPrf(this);

        // Untuk nunjukin intro slide di update 8.1.0
        SharedPreferences prf = getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        boolean seeIntroSlide = prf.getBoolean(AppConst.TAG_SEE_INTRO_SLIDE, true);

        if (!seeIntroSlide) {

            if (!customer_id.equals("")) {

                // Login API, for Checking Credential
                ApiEndPoint loginApiEndPoint = Utility.getClient(customer_id, password)
                        .create(ApiEndPoint.class);
                Call<LoginResponse> call = loginApiEndPoint.authCheck(Utility.getCustRegId(this));
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        LoginResponse loginResponse = response.body();
                        if (loginResponse != null) {
                            if (loginResponse.isSuccess()) {
                                Intent i = new Intent(SplashActivity.this, ControllerActivity.class);
                                i.putExtra(AppConst.PARC_LOGIN_RESP, loginResponse);
                                startActivity(i);
                                finish();

                            } else {

                                doReLogin();
                            }

                        } else {
                            if (response.code() == AppConst.UNAUTHORIZED) doReLogin();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        t.printStackTrace();
                        if (t instanceof IOException) {

                            // i do this because condition -> if (networkInfo != null && networkInfo.isConnected())
                            // not working on android > kitkat
                            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                            builder.setMessage(AppConst.NO_CONNECTION_MSG)
                                    .setTitle("Mangoesky")
                                    .setCancelable(false)
                                    .setNegativeButton("Batal",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {

                                                    dialog.cancel();
                                                    System.exit(0);
                                                }
                                            })
                                    .setPositiveButton("Coba kembali",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int id) {

                                                    dialog.cancel();
                                                    checkInternet();
                                                }
                                            })

                                    .show();
                        }
                    }
                });

            } else {

                int SPLASH_DISPLAY_LENGTH = 2000;
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, IntroslideActivity.class));
                        finish();
                    }
                }, SPLASH_DISPLAY_LENGTH);
            }

        } else {

            SharedPreferences.Editor editor = prf.edit();
            seeIntroSlide = false;
            editor.putBoolean(AppConst.TAG_SEE_INTRO_SLIDE, seeIntroSlide);
            editor.apply();

            Utility.emptyCustPrf(this);

            int SPLASH_DISPLAY_LENGTH = 2000;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, IntroslideActivity.class));
                    finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    private void doReLogin() {

        Utility.emptyCustPrf(this);

        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

}
