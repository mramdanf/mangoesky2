package com.mangoesky.activity;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.mangoesky.R;
import com.mangoesky.apiendpoint.ApiEndPoint;
import com.mangoesky.apiresponsewrapper.BuyTopupResponse;
import com.mangoesky.fragment.DetailTrancationFragment;
import com.mangoesky.fragment.TopupInvoiceFragment;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.Payment;
import com.mangoesky.model.TopupPacketModel;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentConfirmActivity extends AppCompatActivity implements
        DetailTrancationFragment.DetailTransactionFragmentInf,
        TopupInvoiceFragment.TopupInvoiceFragmentInf {

    TopupPacketModel packet;
    String custIdFromPrf;
    String passFromPrf;
    private ProgressDialog pDialog;
    private Context mContext;
    private final String LOG_TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirm);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        custIdFromPrf = Utility.getCustIdPrf(this);
        passFromPrf = Utility.getCustPassPrf(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.please_wait));
        pDialog.setCancelable(false);

        mContext = this;

        if (getIntent().hasExtra(AppConst.INTENT_EX_PACKET)) {
            Bundle args = getIntent().getBundleExtra(AppConst.INTENT_EX_PACKET);

            packet = args.getParcelable(AppConst.PARC_PACKET);

            DetailTrancationFragment f = new DetailTrancationFragment();
            f.setArguments(args);
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.payment_confirm_fragmentc, f);
            fragmentTransaction.commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void setUpFragment(Fragment f) {
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.payment_confirm_fragmentc, f);
        String tag = fragmentTransaction.toString();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    @Override
    public void DetailTransactionClickListener(int viewId) {
        switch (viewId) {
            case R.id.go_back_btn:
                super.onBackPressed();
                break;
            case R.id.go_paid_btn:
                goPaidForTopupWithApi();
                break;
            default:
                break;
        }
    }

    private void goPaidForTopupWithApi() {
        if (packet != null) {
            pDialog.show();
            ApiEndPoint endPoint = Utility.getClient(custIdFromPrf, passFromPrf)
                    .create(ApiEndPoint.class);
            Call<BuyTopupResponse> call = endPoint.goForPaidTopupApi(packet.getId());
            call.enqueue(new Callback<BuyTopupResponse>() {
                @Override
                public void onResponse(Call<BuyTopupResponse> call, Response<BuyTopupResponse> response) {
                    pDialog.dismiss();
                    BuyTopupResponse result = response.body();
                    if (result != null) {

                        Payment payment = result.getPayment();
                        if (result.isSuccess()) {

                            String expDate = "";
                            String expTime = "";
                            try {
                                String unFormatExp = payment.getExpiration();
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                                Date newDate = format.parse(unFormatExp);

                                format = new SimpleDateFormat("dd MMMM yyy", Locale.US);
                                expDate = format.format(newDate);

                                format = new SimpleDateFormat("hh:mm:ss", Locale.US);
                                expTime = format.format(newDate);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Bundle args = new Bundle();
                            args.putString(AppConst.TAG_INVOICE, payment.getInvoice());
                            args.putString(AppConst.TAG_NAME, packet.getName());
                            args.putString(AppConst.TAG_PRICE, packet.getPrice());
                            args.putString(AppConst.TAG_CODE, payment.getCode());
                            args.putString(AppConst.TAG_DATE, expDate);
                            args.putString(AppConst.TAG_TIME, expTime);

                            TopupInvoiceFragment f = new TopupInvoiceFragment();
                            f.setArguments(args);
                            setUpFragment(f);

                        } else {
                            Utility.displayAlert(mContext, result.getMsg());
                        }

                    } else {
                        if (response.code() == AppConst.UNAUTHORIZED) {
                            Utility.doReLogin(mContext);
                        }
                    }
                }

                @Override
                public void onFailure(Call<BuyTopupResponse> call, Throwable t) {
                    pDialog.dismiss();
                    t.printStackTrace();
                    if (t instanceof IOException) {
                        Utility.displayAlert(mContext, getString(R.string.msg_no_internet));
                    }
                }
            });
        }
    }

    @Override
    public void topupInvoiceClickListener(Bundle args) {
        if (args != null) {
            int viewId = args.getInt(AppConst.TOPUPINV_CLICK_ID);
            switch (viewId) {
                case R.id.btn_finish_order:
                    finish();
                    break;
                case R.id.copy_pc:
                    String paymentCode = args.getString(AppConst.TAG_CODE);
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clipData = ClipData.newPlainText("text", paymentCode);
                    clipboard.setPrimaryClip(clipData);
                    Toast.makeText(mContext, getString(R.string.copied_code_text), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.btn_how_topay:
                    startActivity(new Intent(mContext, TopupPaymentManualActivity.class));
                    break;
                case R.id.btn_list_topup:
                    Intent i = new Intent(mContext, ControllerActivity.class);
                    i.putExtra(AppConst.TAG_FLAG, getClass().getSimpleName());
                    setResult(AppConst.FINPAY_RESULT_SEE_TOPUP_HISTORY, i);
                    finish();
                    break;
                default:
                    Log.d(LOG_TAG, "undefined viewId");
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
