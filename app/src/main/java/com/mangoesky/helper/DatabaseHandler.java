package com.mangoesky.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mangoesky.model.Banner;
import com.mangoesky.model.MyNotif;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 20/3/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "dbNotif";

    // Contacts table name
    private static final String TABLE_NOTIF = "tblNotif";
    private static final String TABLE_BANNER = "tblBanner";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String NOTIF_TITLE = "notif_title";
    private static final String NOTIF_BODY = "notif_body";
    private static final String IS_READ = "is_read";
    private static final String RECIVED_AT = "recieved_at";
    private static final String CUSTOMER_ID = "customer_id";

    private static final String IMG_ID = "img_id";
    private static final String IMG_MD5 = "img_md5";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_NOTIF_TABLE = "CREATE TABLE " + TABLE_NOTIF + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + NOTIF_TITLE + " TEXT,"
                + NOTIF_BODY + " TEXT,"
                + IS_READ + " TEXT,"
                + CUSTOMER_ID + " TEXT,"
                + RECIVED_AT + " TEXT" + ")";
        db.execSQL(CREATE_NOTIF_TABLE);

        String CREATE_BANNER_TABLE = "CREATE TABLE " + TABLE_BANNER + "("
                + IMG_ID + " INTEGER PRIMARY KEY," + IMG_MD5 + " TEXT"
                + ")";
        db.execSQL(CREATE_BANNER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIF);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANNER);

        // Create tables again
        onCreate(db);

    }


    public void addNotif(MyNotif myNotif){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NOTIF_TITLE, myNotif.getNotifTitle());
        values.put(NOTIF_BODY, myNotif.getNotifBody());
        values.put(IS_READ, myNotif.getIsRead());
        values.put(CUSTOMER_ID, myNotif.getCustomer_id());
        values.put(RECIVED_AT, myNotif.getRecievedAT());


        db.insert(TABLE_NOTIF, null, values);
        db.close();
    }

    public void addBanner(Banner banner) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IMG_ID, banner.getImgId());
        values.put(IMG_MD5, Utility.md5(banner.getImgMd5()));


        db.insert(TABLE_BANNER, null, values);
        db.close();
    }

    public MyNotif getNotif(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NOTIF, new String[]{KEY_ID, NOTIF_TITLE,
                        NOTIF_BODY, IS_READ, CUSTOMER_ID, RECIVED_AT}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) cursor.moveToFirst();

        MyNotif myNotif = new MyNotif();
        myNotif.setId(cursor.getInt(0));
        myNotif.setNotifTitle(cursor.getString(1));
        myNotif.setNotifBody(cursor.getString(2));
        myNotif.setIsRead(cursor.getString(3));
        myNotif.setCustomer_id(cursor.getString(4));
        myNotif.setRecievedAT(cursor.getString(5));


        return myNotif;
    }

    public List<MyNotif> getAllNotif(String cust_id){
        List<MyNotif> myNotifList = new ArrayList<MyNotif>();

        String query = "SELECT * FROM " + TABLE_NOTIF + " WHERE " + CUSTOMER_ID + " = '" + cust_id
                + "' ORDER BY " + KEY_ID + " DESC";

//        String query = "SELECT * FROM " + TABLE_NOTIF + " ORDER BY " + IS_READ + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            do{
                MyNotif myNotif = new MyNotif();
                myNotif.setId(cursor.getInt(0));
                myNotif.setNotifTitle(cursor.getString(1));
                myNotif.setNotifBody(cursor.getString(2));
                myNotif.setIsRead(cursor.getString(3));
                myNotif.setCustomer_id(cursor.getString(4));
                myNotif.setRecievedAT(cursor.getString(5));


                myNotifList.add(myNotif);
            } while (cursor.moveToNext());
        }

        return myNotifList;
    }

    public List<Banner> getAllBanner(){

        List<Banner> bannerList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_BANNER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            do{
                Banner banner = new Banner();
                banner.setImgId(cursor.getInt(0));
                banner.setImgMd5(cursor.getString(1));


                bannerList.add(banner);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return bannerList;
    }

    public Banner getBanner(int imgId) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_BANNER, new String[]{IMG_ID, IMG_MD5}, IMG_ID + "=?",
                new String[]{String.valueOf(imgId)}, null, null, null, null);

        if (cursor != null) cursor.moveToFirst();

        Banner banner = new Banner();
        banner.setImgId(cursor.getInt(0));
        banner.setImgMd5(cursor.getString(0));

        cursor.close();

        return banner;
    }

    public int updateBanner(int imgId, Banner newBanner){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IMG_MD5, Utility.md5(newBanner.getImgMd5()));

        return db.update(TABLE_BANNER, values, IMG_ID + " = ?",
                new String[] {String.valueOf(imgId)});
    }

    public void deleteAllBanner(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_BANNER);

        db.close();
    }

    public int setIsReadYes(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(IS_READ, "yes");

        return db.update(TABLE_NOTIF, values, KEY_ID + " = ?",
                new String[] {String.valueOf(id)});
    }

    public void deleteSingleNotif(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIF, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public void deleteAllNotif(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + TABLE_NOTIF);

        db.close();
    }

    public void dropTable(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE dbNotif.tblNotif ");

        db.close();
    }
}
