package com.mangoesky.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.mangoesky.R;

/**
 * Created by root on 2/3/17.
 */

public class TextProgressBar extends ProgressBar {
    private String text;
    private Paint textPaint;
    private Context c;

    public TextProgressBar(Context context) {
        super(context);
        c = context;
        text = "";
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);

    }

    public TextProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        c = context;
        text = "";
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
    }

    public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        c = context;
        text = "";
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        // First draw the regular progress bar, then custom draw our text
        super.onDraw(canvas);
        Rect bounds = new Rect();
        this.setProgressDrawable(c.getResources().getDrawable(R.drawable.orange_progressbar));
        textPaint.setTextSize(19.0f);
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        int x = getWidth() / 2 - bounds.centerX();
        int y = getHeight() / 2 - bounds.centerY();
        canvas.drawText(text, x, y, textPaint);
    }

    public synchronized void setText(String text) {
        this.text = text;
        drawableStateChanged();
    }

    public void setTextColor(int color) {
        textPaint.setColor(color);
        drawableStateChanged();
    }
}
