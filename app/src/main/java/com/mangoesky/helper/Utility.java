package com.mangoesky.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.activity.IntroslideActivity;
import com.mangoesky.activity.LoginActivity;
import com.mangoesky.apiendpoint.ApiEndPoint;
import com.mangoesky.model.SimpleModel;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.PUT;

import static android.content.Context.MODE_PRIVATE;

/**
 * Class which has Utility methods
 * 
 */
public class Utility {

    private static Retrofit retrofitClient;

    public static void displayAlert(Context c, String msg){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(c);

        LayoutInflater layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);
        TextView tvCustomAlert = (TextView) dialogView.findViewById(R.id.tv_custom_alert);
        tvCustomAlert.setText(msg);

        builder
                .setView(dialogView)
                .setTitle(AppConst.TITLE_MANGOESKY)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                dialog.cancel();
                            }
                        }).show();

    }

    public static void doReLogin(final Context c) {

        new android.support.v7.app.AlertDialog.Builder(c)
                .setTitle(AppConst.TITLE_MANGOESKY)
                .setMessage(c.getString(R.string.msg_relogin))
                .setCancelable(false)
                .setPositiveButton(c.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Utility.logoutWithApi(c);
                    }
                }).show();
    }

    public static String getCustIdPrf(Context c) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        return prfs.getString(AppConst.PRF_KEY_CUSTID, "");
    }

    public static String getCustPassPrf(Context c) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        return prfs.getString(AppConst.PRF_KEY_CUSTPASS, "");
    }

    public static String getCustRegId(Context c) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        return prfs.getString(AppConst.PRF_KEY_REGID, "");
    }

    public static void setCustIdPrf(Context c, String newCustId) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prfs.edit();
        editor.putString(AppConst.PRF_KEY_CUSTID, newCustId);
        editor.apply();
    }

    public static void setCustPassPrf(Context c, String newCustPass) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prfs.edit();
        editor.putString(AppConst.PRF_KEY_CUSTPASS, newCustPass);
        editor.apply();
    }

    public static void setCustRegIdPrf(Context c, String newCustRegId) {
        SharedPreferences prfs = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prfs.edit();
        editor.putString(AppConst.PRF_KEY_REGID, newCustRegId);
        editor.apply();
    }

    public static void emptyCustPrf(Context c) {
        SharedPreferences preferences = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppConst.PRF_KEY_CUSTID, "");
        editor.putString(AppConst.PRF_KEY_CUSTPASS, "");
        editor.putString(AppConst.PRF_KEY_REGID, "");
        editor.apply();
    }

    public static void setCustFullnamePrf(Context c, String fullname) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = prf.edit();
        editor.putString(AppConst.TAG_FULLNAME, fullname);
        editor.apply();
    }

    public static String getCustFullnamePrf(Context c) {
        SharedPreferences prf = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        return prf.getString(AppConst.TAG_FULLNAME, "");
    }

    public static void emptCustFullnamePrf(Context c) {
        SharedPreferences preferences = c.getSharedPreferences(AppConst.PRF_CUST, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppConst.TAG_FULLNAME, "");
        editor.apply();
    }

    public static Retrofit getClient(String customerId, String password){

        Retrofit retrofit;

        retrofit = null;

        if(customerId != null && password != null) {
            String credentials = customerId + ":" + password;

            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            /*HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(httpLoggingInterceptor);*/

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .header("User-Agent", "Mangoesky Android 8.1.1")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.connectTimeout(60, TimeUnit.SECONDS);

            OkHttpClient client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConst.BASE_URL_V2)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public static String thousandSparator(String data){
        int dataInt = Integer.valueOf(data);
        return String.format(Locale.US, "%,d", dataInt);
    }

    public static void displayNoIntAlert(Context c) {
        displayAlert(c, c.getString(R.string.msg_no_internet));
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Retrofit getDummyClient(String customerId, String password){

        Retrofit retrofit;

        retrofit = null;

        if(customerId != null && password != null) {
            String credentials = customerId + ":" + password;

            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            /*HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(httpLoggingInterceptor);*/

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.connectTimeout(60, TimeUnit.SECONDS);

            OkHttpClient client = builder.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://www.mangoesky.com/api_v2/apps2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public static String getGreetingText(Context ctx) {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        String greetingText = "";

        if(timeOfDay >= 0 && timeOfDay < 12){
            //Toast.makeText(this, "Good Morning", Toast.LENGTH_SHORT).show();
            greetingText = ctx.getString(R.string.good_morning);
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            //Toast.makeText(this, "Good Afternoon", Toast.LENGTH_SHORT).show();
            greetingText = ctx.getString(R.string.good_afternoon);
        }else if(timeOfDay >= 16 && timeOfDay < 19){
            //Toast.makeText(this, "Good Evening", Toast.LENGTH_SHORT).show();
            greetingText = ctx.getString(R.string.good_evening);
        }else if(timeOfDay >= 19 && timeOfDay < 24){
            //Toast.makeText(this, "Good Night", Toast.LENGTH_SHORT).show();
            greetingText = ctx.getString(R.string.good_night);
        }

        return greetingText;
    }

    private static void logoutWithApi(final Context c) {

        final ProgressDialog progressDialog = new ProgressDialog(c);

        progressDialog.setMessage(c.getString(R.string.please_wait));

        progressDialog.show();

        if (retrofitClient == null) {
            retrofitClient = Utility.getClient(Utility.getCustIdPrf(c), Utility.getCustPassPrf(c));
        }

        ApiEndPoint endPoint = retrofitClient.create(ApiEndPoint.class);
        Call<SimpleModel> call = endPoint.logoutApi(Utility.getCustRegId(c));
        call.enqueue(new Callback<SimpleModel>() {
            @Override
            public void onResponse(Call<SimpleModel> call, retrofit2.Response<SimpleModel> response) {
                progressDialog.dismiss();

                SharedPreferences preferences = c.getApplicationContext().getSharedPreferences("customer", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("customer_id", "");
                editor.putString("password", "");
                editor.putString("regId", "");
                editor.apply();

                c.startActivity(new Intent(c, LoginActivity.class));
                ((Activity)c).finish();
            }

            @Override
            public void onFailure(Call<SimpleModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                if (t instanceof IOException) {
                    Utility.displayAlert(c, c.getString(R.string.msg_no_internet));
                }
            }
        });
    }
}
