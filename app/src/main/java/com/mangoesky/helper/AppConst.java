package com.mangoesky.helper;

public interface AppConst {


	String MANGO_DATA = "mangodata";

	// Google Project Number
	String GOOGLE_PROJ_ID = "592187080763";

	// Message Key
	String MSG_KEY = "message";
	String MSG_TYPE = "type";

	String MSG_TITLE = "title";
	String TITLE_MANGOESKY = "Mangoesky";
	String NO_CONNECTION_MSG = "Koneksi gagal, mohon periksa koneksi internet anda atau coba beberapa saat lagi.";

	int UNAUTHORIZED = 401;

	String MSG = "msg";

	// sharedpreferance
	String PRF_CUST = "customer";
	String PRF_KEY_CUSTID = "customer_id";
	String PRF_KEY_CUSTPASS = "password";
	String PRF_KEY_REGID = "regId";

	// Post Parameters
	String POST_REGID = "reg_id";
	String POST_FULLNAME = "fullname";
	String POST_GENDER = "gender";
	String POST_PHONE = "phone";
	String POST_EMAIL = "email";
	String POST_ADDRESS = "address";
	String POST_VOUCHER_CODE = "voucher-code";

	// Bundle Key
	String PARC_HISTORY_RESP = "parc_history_resp";
	String SELECTED_DATE = "selected-date";
	String PARC_TOPUP_PACKET_RESP = "parc-topup-packet-resp";
	String PARC_USER = "parc-user";
	String OLD_PASSWORD = "old_password";
	String NEW_PASSWORD = "new_password";
	String CONFIRM_NEW_PASSWORD = "confirm_new_password";
	String PARC_CC_RESP = "parc-cc-resp";
	String PARC_SERVICE_RESP = "parc-service-resp";
	String PARC_PACKET = "parc-packet";
	String PARC_PACKET_TOPUPHISTORY = "parc-topup-history";
	String TOPUPINV_CLICK_ID = "topupinv-click";
	String ITEM_ID = "item-id";

	// Intent extra
	String PARC_LOGIN_RESP = "parc-login-resp";

	// For Gcm receiver
	String FILTER_STRING = "filter-string";

	String BASE_URL = "https://www.mangoesky.com/api_android/";
	String BASE_URL_V2 = "https://www.mangoesky.com/api_v2/apps/";
	String BASE_URL_BACKUP = "https://database.metrasat.net/crm_consumer/index.php/api/apps/";

	String FROM_NOTIF = "from-notif";

	String ElEVANIA_URL = "http://www.elevenia.co.id/product/ProductDetailAction/" +
			"getProductDetail.do?prdNo=";

	String ARGS_TEXTVIEW_VOUCHERCODE = "tv_voucher";

	String INTENT_EX_PACKET = "intent-ex-packet";
	String TAG_FULLNAME = "fullname";
	String TAG_PAYMENT_METHODE = "payment-methode";
	String TAG_TOPUP_PACKET_ID = "topup_packet_id";
	String TAG_SEE_INTRO_SLIDE = "see-intro-slide";

	String TAG_INVOICE = "invoice";
	String TAG_CODE = "code";
	String TAG_DATE = "tag-date";
	String TAG_TIME = "tag-time";
	String TAG_PRICE = "price";
	String TAG_NAME = "name";
	String TAG_FLAG = "flag";
	String TAG_TITLE_USER_INFO = "tag-title-user-info";
	String URL_PAYMENT_MANUAL_FINNET = "https://www.mangoesky.com/topup/how_to_pay_finnet";
	int TOPUP_HISTORY_ITEMPAGE = 4;
	String TAG_CURR_PAGE = "current-page";
	int FINPAY_ACTION_TAG = 2;
	int SEE_INVOICE_ACTION_TAG = 3;
	int FINPAY_ACTIVITY_REQUEST = 5;
	int FINPAY_RESULT_SEE_TOPUP_HISTORY = 6;
}
