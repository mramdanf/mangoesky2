package com.mangoesky.helper;

import android.util.Log;

import com.mangoesky.model.TopupHistory;

import java.util.ArrayList;
import java.util.List;

public class Paginator {
    private int totalNumItems;
    private int itemPerPage = AppConst.TOPUP_HISTORY_ITEMPAGE;
    private int totalPage;
    private List<TopupHistory> topupHistoryList;
    private final String LOG_TAG = getClass().getSimpleName();

    public Paginator(List<TopupHistory> topupHistoryList) {
        this.totalNumItems = topupHistoryList.size();
        this.topupHistoryList = topupHistoryList;

        if (totalNumItems % itemPerPage == 0)
            totalPage = totalNumItems/itemPerPage;
        else
            totalPage = totalNumItems/itemPerPage + 1;
    }

    public List<TopupHistory> generatePage(int currPage) {
        int startItem = currPage * itemPerPage;

        List<TopupHistory> pageData = new ArrayList<>();

        for (int i = startItem; i < totalNumItems; i++) {

            if (i == itemPerPage + startItem) break;

            pageData.add(topupHistoryList.get(i));
        }

        return pageData;
    }

    public int getTotalPage() {
        return totalPage - 1;
    }
}
