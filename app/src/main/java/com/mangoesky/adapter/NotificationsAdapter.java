package com.mangoesky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.model.MyNotif;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ramdan Firdaus on 13/2/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.RecyclerViewHolder> {

    private List<MyNotif> notifList;
    private Context mContext;

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_notif)
        TextView notifTitle;
        @BindView(R.id.body_notif)
        TextView notifBody;
        @BindView(R.id.time_notif)
        TextView notifTime;
        @BindView(R.id.date_notif)
        TextView notifDate;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public NotificationsAdapter(Context mContext, List<MyNotif> notifList) {
        this.notifList = notifList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_item, parent, false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        MyNotif notif = notifList.get(position);
        holder.notifTitle.setText(notif.getNotifTitle());
        holder.notifBody.setText(notif.getNotifBody());
        holder.notifDate.setText(notif.getRecievedAT().substring(0, 10));
        holder.notifTime.setText(notif.getRecievedAT().substring(11));
    }

    @Override
    public int getItemCount() {
        return notifList.size();
    }
}
