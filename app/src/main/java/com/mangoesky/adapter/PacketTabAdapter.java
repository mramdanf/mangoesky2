package com.mangoesky.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.model.HistoryTabModel;

import java.util.List;

/**
 * Created by user on 23/5/2016.
 */
public class PacketTabAdapter extends RecyclerView.Adapter<PacketTabAdapter.MyViewHolder> {

    private List<HistoryTabModel> packetList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView packetDesc, packetType, packetAbodemen;

        public MyViewHolder(View itemView) {
            super(itemView);
            packetDesc = (TextView) itemView.findViewById(R.id.sc_speed);
            packetType = (TextView) itemView.findViewById(R.id.sc_time);
            packetAbodemen = (TextView) itemView.findViewById(R.id.packet_abodemen);
        }
    }

    public PacketTabAdapter(List<HistoryTabModel> mPakcetList){
        this.packetList = mPakcetList;

    }

    @Override
    public PacketTabAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.packet_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PacketTabAdapter.MyViewHolder holder, int position) {
        HistoryTabModel historyTabModel = this.packetList.get(position);
        holder.packetDesc.setText(historyTabModel.getPacketDescription());
        holder.packetType.setText(historyTabModel.getPacketType());
        holder.packetAbodemen.setText(historyTabModel.getPacketAbodemen());

    }

    @Override
    public int getItemCount() {
        return this.packetList.size();
    }

}
