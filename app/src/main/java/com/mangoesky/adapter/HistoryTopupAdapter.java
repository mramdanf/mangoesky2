package com.mangoesky.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.TopupHistory;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ramdan Firdaus on 8/11/2016.
 */
public class HistoryTopupAdapter extends RecyclerView.Adapter<HistoryTopupAdapter.MyViewHolder> {

    private List<TopupHistory> topupHistoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.history_invoice) TextView historyInvoice;
        @BindView(R.id.history_payment_methode) TextView historyPaymentMethode;
        @BindView(R.id.history_packet_topup) TextView historyPacketTopup;
        @BindView(R.id.history_status) TextView historyStatus;
        @BindView(R.id.history_payment) TextView historyPayment;
        @BindView(R.id.tv_buy_date) TextView tvBuyDate;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public HistoryTopupAdapter(List<TopupHistory> topupHistoryList) {
        this.topupHistoryList = topupHistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_topup_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TopupHistory topupHistory = topupHistoryList.get(position);
        holder.historyInvoice.setText(topupHistory.getInvoice());
        holder.historyPaymentMethode.setText(topupHistory.getType());
        holder.historyPacketTopup.setText(topupHistory.getTopupPacketName());
        holder.historyStatus.setText(topupHistory.getStatus());
        holder.historyPayment.setText("Rp. "+ Utility.thousandSparator(topupHistory.getPrice()));
        holder.tvBuyDate.setText(topupHistory.getBuyDateFmt() + " " + topupHistory.getTimeBuy());
    }

    @Override
    public int getItemCount() {
        return topupHistoryList.size();
    }


}
