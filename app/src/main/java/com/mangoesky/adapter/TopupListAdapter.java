package com.mangoesky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.TopupPacketModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ramdan Firdaus on 8/2/2017.
 */

public class TopupListAdapter extends RecyclerView.Adapter<TopupListAdapter.MyViewHolder> {

    private Context mContext;
    private List<TopupPacketModel> topupPacketModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_topup_size)
        TextView titleTopupSize;
        @BindView(R.id.topup_thumbnail)
        ImageView imgTopupThumbnail;
        @BindView(R.id.topup_price_text)
        TextView topupPriceText;
        @BindView(R.id.topup_img_progress)
        ProgressBar topupImgProgress;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public TopupListAdapter(Context mContext, List<TopupPacketModel> topupPacketModelList) {
        this.mContext = mContext;
        this.topupPacketModelList = topupPacketModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topup_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TopupPacketModel topupPacketModel = topupPacketModelList.get(position);

        String price = "Rp. " + Utility.thousandSparator(topupPacketModel.getPrice());
        String topupDesc = mContext.getString(R.string.unlimited) + " " + topupPacketModel.getName();

        holder.titleTopupSize.setText(topupDesc);
        holder.topupPriceText.setText(price);
        Picasso
                .with(mContext)
                .load(topupPacketModel.getBlanjaItemImgUrl())
                .resize(700, 200)
                .centerInside()
                .into(holder.imgTopupThumbnail, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.topupImgProgress.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public int getItemCount() {
        return topupPacketModelList.size();
    }
}
