package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.TopupPacketModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailTrancationFragment extends Fragment {

    @BindView(R.id.detail_cust_id)
    TextView custId;
    @BindView(R.id.detail_cust_name) TextView custName;
    @BindView(R.id.detail_pay_methode) TextView payMethode;
    @BindView(R.id.detail_date) TextView detailDate;
    @BindView(R.id.detail_price) TextView detailPrice;
    @BindView(R.id.detail_kuota) TextView kuota;

    private DetailTransactionFragmentInf mCallback;


    public DetailTrancationFragment() {
        // Required empty public constructor
    }

    public interface DetailTransactionFragmentInf {
        void DetailTransactionClickListener(int viewId);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail_trancation, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();

        if (args != null){

            TopupPacketModel packet = args.getParcelable(AppConst.PARC_PACKET);

            custId.setText(Utility.getCustIdPrf(getActivity()));
            custName.setText(args.getString(AppConst.TAG_FULLNAME));
            payMethode.setText(args.getString(AppConst.TAG_PAYMENT_METHODE));

            getActivity().setTitle(args.getString(AppConst.TAG_PAYMENT_METHODE));

            Date date = new Date();
            SimpleDateFormat writeFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

            detailDate.setText( writeFormat.format(date));

            if (packet != null) {
                detailPrice.setText(Utility.thousandSparator(packet.getPrice()));
                kuota.setText(packet.getName());
            }

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (DetailTransactionFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " Activity must implement DetailTransactionFragmentInf");
        }

    }

    @OnClick(R.id.go_back_btn) public void backPresed() {
        mCallback.DetailTransactionClickListener(R.id.go_back_btn);
    }

    @OnClick(R.id.go_paid_btn) public void goPaid() {
        mCallback.DetailTransactionClickListener(R.id.go_paid_btn);
    }
}
