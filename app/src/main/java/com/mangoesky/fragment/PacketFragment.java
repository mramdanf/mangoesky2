package com.mangoesky.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.PacketTabAdapter;
import com.mangoesky.apiresponsewrapper.HistoryResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.model.HistoryTabModel;
import com.mangoesky.model.PacketSubscr;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PacketFragment extends Fragment {

    private List<HistoryTabModel> listPacket = new ArrayList<>();
    private RecyclerView recyclerView;
    private PacketTabAdapter mAdapter;
    private TextView tvDate;
    private final String LOG_TAG = this.getClass().getSimpleName();


    public PacketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_packet, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_packet);
        mAdapter = new PacketTabAdapter(listPacket);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        tvDate = (TextView) rootView.findViewById(R.id.tv_date);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            tvDate.setText(args.getString(AppConst.SELECTED_DATE));
            HistoryResponse h = args.getParcelable(AppConst.PARC_HISTORY_RESP);
            preparePacketData(h);
        }
    }


    private void preparePacketData(HistoryResponse h) {
        List<PacketSubscr> packetSubscrList = h.getPacketSubscrList();

        listPacket.clear();
        String desc = packetSubscrList.get(0).getName();
        String type =  packetSubscrList.get(0).getType();
        String abodemen =  packetSubscrList.get(0).getAbonemen();
        HistoryTabModel historyTabModel = new HistoryTabModel(1,0,0,desc, type, abodemen, "", "","", "", "");
        listPacket.add(historyTabModel);
    }
}
