package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.HistoryPagerAdapter;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryContentFragment extends Fragment {

    @BindView(R.id.history_pager) ViewPager viewPager;
    @BindView(R.id.history_tab) TabLayout tabLayout;
    @BindView(R.id.user_info_custid) TextView tvCustId;
    @BindView(R.id.user_info_fullname) TextView tvFullname;
    @BindView(R.id.user_info_title) TextView tvTitle;

    private final String LOG_TAG = HistoryContentFragment.class.getSimpleName();
    private HistoryContentInf mCallback;

    public HistoryContentFragment() {
        // Required empty public constructor
    }

    public interface HistoryContentInf {
        void historyContentClickListener(Bundle args);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_content_history, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            setUpViewPager(viewPager, args);
            tabLayout.setupWithViewPager(viewPager);

            // Populate user info
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvFullname.setText(user.getFullname());
                tvCustId.setText(Utility.getCustIdPrf(getActivity()));
            }

            // Screen title
            tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));

        } else {
            Utility.displayAlert(getActivity(), getString(R.string.history_null));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mCallback = (HistoryContentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HistoryContentInf.");
        }
    }

    @OnClick(R.id.new_btn_back) public void goBack(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.historyContentClickListener(args);
    }

    @OnClick(R.id.humb_content_history) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.historyContentClickListener(args);
    }

    private void setUpViewPager(ViewPager viewPager, Bundle args){

        HistoryPagerAdapter historyPagerAdapter = new
                HistoryPagerAdapter(getChildFragmentManager());

        PacketFragment packetFragment = new PacketFragment();
        TopupFragment topupFragment = new TopupFragment();
        SpeedChangeFragment speedChangeFragment = new SpeedChangeFragment();

        packetFragment.setArguments(args);
        topupFragment.setArguments(args);
        speedChangeFragment.setArguments(args);

        historyPagerAdapter.addFragment(packetFragment, "Packet");
        historyPagerAdapter.addFragment(topupFragment, "Topup");
        historyPagerAdapter.addFragment(speedChangeFragment, "Speed Change");
        viewPager.setAdapter(historyPagerAdapter);
    }

}
