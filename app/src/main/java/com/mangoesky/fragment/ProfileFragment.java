package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.User;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.profile_tv_email)
    TextView profileEmail;
    @BindView(R.id.profile_tv_phone)
    TextView profilePhone;
    @BindView(R.id.profile_tv_gender)
    TextView profileGender;
    @BindView(R.id.profile_tv_add)
    TextView profileAddress;
    @BindView(R.id.profile_tv_fullname)
    TextView profileFullname;
    @BindView(R.id.profile_tv_custid)
    TextView profileCustId;
    @BindView(R.id.profile_header)
    ImageView imageViewProfileHeader;

    private ProfileFragmentInf mCallback;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public interface ProfileFragmentInf {
        void onItemClickListener(int itemId);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            User user = args.getParcelable(AppConst.PARC_USER);

            if (user != null) {
                profileFullname.setText(user.getFullname());
                profileEmail.setText(user.getEmail());
                profilePhone.setText(user.getPhone());

                String gender = user.getGender();
                if (TextUtils.equals(user.getGender(), "M")) gender = getString(R.string.text_male);
                else if (TextUtils.equals(user.getGender(), "F")) gender = getString(R.string.text_famale);

                profileGender.setText(gender);

                profileAddress.setText(user.getAddress());
                profileCustId.setText(Utility.getCustIdPrf(getActivity()));
            }

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ProfileFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implment ProfileFragmentInf.");
        }
    }

    @OnClick(R.id.btn_go_editprofile) public void goEditProfile(View v) {
        mCallback.onItemClickListener(v.getId());
    }

    @OnClick(R.id.btn_go_editpass) public void goChangePassword(View v) {
        mCallback.onItemClickListener(v.getId());
    }

    @OnClick(R.id.humb_view_profile) public void showNavigationDrawer(View v) {
        mCallback.onItemClickListener(v.getId());
    }
}
