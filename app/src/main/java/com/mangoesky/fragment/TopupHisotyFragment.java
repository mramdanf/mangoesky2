package com.mangoesky.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.HistoryTopupAdapter;
import com.mangoesky.apiresponsewrapper.TopupHistoryResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Paginator;
import com.mangoesky.helper.RecyclerItemClickListener;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.TopupHistory;
import com.mangoesky.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopupHisotyFragment extends Fragment
        implements RecyclerItemClickListener.OnItemClickListener{

    private List<TopupHistory> topupHistoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private final String LOG_TAG = getClass().getSimpleName();
    private String customerId, password;
    private Paginator p;
    private int currentPage = 0;
    private int totalPage = 0;
    private TopupHisotryFragmentInf mCallback;

    @BindView(R.id.nextBtn) Button nextBtn;
    @BindView(R.id.prevBtn) Button prevBtn;
    @BindView(R.id.firstBtn) Button firstPage;
    @BindView(R.id.lastBtn) Button lastPage;
    @BindView(R.id.list_page) TextView listPage;
    @BindView(R.id.tv_empty_history_topup) TextView tvEmptyHistoryTopup;
    @BindView(R.id.wrapper_pagination_btn) LinearLayout wrapperPaginationBtn;
    @BindView(R.id.user_info_custid) TextView tvCustId;
    @BindView(R.id.user_info_fullname) TextView tvFullname;
    @BindView(R.id.user_info_layout) LinearLayout linearLayoutUserInfo;
    @BindView(R.id.user_info_title) TextView tvTitle;


    public TopupHisotyFragment() {
        // Required empty public constructor
    }

    public interface TopupHisotryFragmentInf {
        void topupHistoryFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_topup_hisoty, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), this));

        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            TopupHistoryResponse res = args.getParcelable(AppConst.PARC_PACKET_TOPUPHISTORY);
            if (res != null) {
                topupHistoryList = res.getTopupHistory();
                if (topupHistoryList.size() > 0) {
                    showRecyclerview();
                    p = new Paginator(topupHistoryList);
                    totalPage = p.getTotalPage();
                    currentPage = 0;
                    recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
                    toggleButtons();
                } else {
                    hideRecyclerview();
                }
            }

            // populate user info
            linearLayoutUserInfo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.dark_orange));
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvCustId.setText(Utility.getCustIdPrf(getActivity()));
                tvFullname.setText(user.getFullname());
            }

            // Screen title
            tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));

        }
    }

    @Override
    public void onItemClick(View childView, int position) {

        int newPosition;
        if (currentPage > 0) {
            newPosition = (AppConst.TOPUP_HISTORY_ITEMPAGE * currentPage) + position;
            position = newPosition;
        }

        Bundle args = new Bundle();
        args.putParcelable(AppConst.PARC_PACKET_TOPUPHISTORY, topupHistoryList.get(position));
        args.putInt(AppConst.ITEM_ID, AppConst.SEE_INVOICE_ACTION_TAG);

        SharedPreferences prf = getActivity().getSharedPreferences(AppConst.PRF_CUST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prf.edit();
        editor.putInt(AppConst.TAG_CURR_PAGE, currentPage);
        editor.apply();

        mCallback.topupHistoryFragmentClickListener(args);
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    private void toggleButtons() {
        if (currentPage == totalPage) { // last page
            nextBtn.setEnabled(false);
            prevBtn.setEnabled(true);
            lastPage.setEnabled(false);
            firstPage.setEnabled(true);
        } else if (currentPage == 0) { // first page
            prevBtn.setEnabled(false);
            nextBtn.setEnabled(true);
            firstPage.setEnabled(false);
            lastPage.setEnabled(true);
        } else if (currentPage >= 1 && currentPage <= totalPage) {
            nextBtn.setEnabled(true);
            prevBtn.setEnabled(true);
            firstPage.setEnabled(true);
            lastPage.setEnabled(true);
        }
        listPage.setText("Pages: " + (currentPage+1) + "/" + (totalPage+1));

    }

    @OnClick(R.id.nextBtn)
    public void nextPage() {
        currentPage += 1;

        recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
        toggleButtons();
    }

    @OnClick(R.id.prevBtn)
    public void prevPage() {
        currentPage -= 1;

        recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
        toggleButtons();
    }

    @OnClick(R.id.firstBtn)
    public void goFistPage() {
        currentPage = 0;

        recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
        toggleButtons();
    }

    @OnClick(R.id.lastBtn)
    public void goLastPage() {
        currentPage = totalPage;

        recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
        toggleButtons();
    }

    @OnClick(R.id.humb_topup_history) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.topupHistoryFragmentClickListener(args);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (TopupHisotryFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " Activity must implement TopupHisotryFragmentInf");
        }
    }

    private void showRecyclerview() {
        recyclerView.setVisibility(View.VISIBLE);
        wrapperPaginationBtn.setVisibility(View.VISIBLE);
        tvEmptyHistoryTopup.setVisibility(View.GONE);
    }

    private void hideRecyclerview() {
        recyclerView.setVisibility(View.GONE);
        wrapperPaginationBtn.setVisibility(View.GONE);
        tvEmptyHistoryTopup.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences prf = getActivity().getSharedPreferences(AppConst.PRF_CUST, Context.MODE_PRIVATE);
        int lastCurPage = prf.getInt(AppConst.TAG_CURR_PAGE, 0);

        if (lastCurPage > 0) {

            // jadikan lastcurrpage current page
            currentPage = lastCurPage;
            recyclerView.setAdapter(new HistoryTopupAdapter(p.generatePage(currentPage)));
            toggleButtons();

            // kosongkan prf currpage
            SharedPreferences.Editor editor = prf.edit();
            editor.putInt(AppConst.TAG_CURR_PAGE, 0);
            editor.apply();
        }

    }
}
