package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.User;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private ArrayAdapter adapter;
    private HistoryFragmentIntf mCallback;
    private String year;
    private int month;
    private final String[] monthString = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli",
            "Agustus", "September", "Oktober", "November", "Desember"};
    @BindView(R.id.new_year_spinner) Spinner yearSpinner;
    @BindView(R.id.new_month_spinner) Spinner monthSpinner;
    @BindView(R.id.new_history_progressBar) ProgressBar hisotryPBar;
    @BindView(R.id.user_info_custid) TextView tvCustid;
    @BindView(R.id.user_info_fullname) TextView tvFullname;
    @BindView(R.id.history_header) ImageView imageViewHistoryHeader;


    public HistoryFragment() {
        // Required empty public constructor
    }

    public interface HistoryFragmentIntf {
        void onClickSearchHistory(int month, int year, ProgressBar pBar, String selectedDate,
                                  Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, rootView);

        Picasso
                .with(getActivity())
                .load(R.drawable.img_history_header)
                .into(imageViewHistoryHeader);

        Bundle args = getArguments();
        if (args != null) {
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvFullname.setText(user.getFullname());
                tvCustid.setText(Utility.getCustIdPrf(getActivity()));
            }
        }

        populateMontYearSpinner();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (HistoryFragmentIntf) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                    + " must implement DetailTransactionBtnListener");
        }
    }

    @OnClick(R.id.btn_search_history) public void searchHistory(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());

        year = yearSpinner.getSelectedItem().toString();
        month = monthSpinner.getSelectedItemPosition() + 1;

        mCallback.onClickSearchHistory(month, Integer.valueOf(year), hisotryPBar,
                monthString[month-1] + " " + year, args);
    }

    @OnClick(R.id.humb_history) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.onClickSearchHistory(0, 0, null, "", args);
    }

    private void populateMontYearSpinner() {
        String[] year ;
        Calendar cal = Calendar.getInstance();


        int year_start = 2014;
        year = new String[cal.get(Calendar.YEAR) - year_start + 1];
        for(int i = year_start; i <= cal.get(Calendar.YEAR); i++){
            year[i - year_start] = String.valueOf(i);
        }

        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, year);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(adapter);
        yearSpinner.setSelection(adapter.getPosition(String.valueOf(cal.get(Calendar.YEAR))));

        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, monthString);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(adapter);
        monthSpinner.setSelection(cal.get(Calendar.MONTH));
    }

}
