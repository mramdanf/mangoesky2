package com.mangoesky.fragment;


import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.LocaleDisplayNames;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.apiresponsewrapper.ServiceResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.TextProgressBar;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.Bucket;
import com.mangoesky.model.ServiceModel;
import com.mangoesky.model.User;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    /*@BindView(R.id.quota_pbar)
    TextProgressBar kuotaProgressBar;*/

    @BindView(R.id.text_desc_packet)
    TextView descPacket;
    @BindView(R.id.text_desc_speed)
    TextView descSpeed;
    @BindView(R.id.text_desc_active)
    TextView descActive;
    @BindView(R.id.service_swipe_refresh) SwipeRefreshLayout
    serviceSwipeRefreshLayout;
    @BindView(R.id.humb_service)
    ImageView imgViewHumbService;
    @BindView(R.id.user_info_fullname)
    TextView tvFullname;
    @BindView(R.id.user_info_custid)
    TextView tvCustId;
    @BindView(R.id.user_info_title)
    TextView tvTitle;
    @BindView(R.id.service_header)
    ImageView imageViewServiceHeader;
    @BindView(R.id.wrapper_kuota_progressbar)
    LinearLayout linearLayoutKuotaProgressbar;

    private ServiceFragmentInf mCallback;
    private User user;
    private final String LOG_TAG = getClass().getSimpleName();

    public ServiceFragment() {
        // Required empty public constructor
    }

    public interface ServiceFragmentInf {
        void onSwipeListener();
        void serviceFragmentClicListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_service, container, false);

        ButterKnife.bind(this, rootView);

        serviceSwipeRefreshLayout.setOnRefreshListener(this);
        serviceSwipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(R.color.dark_orange),
                getResources().getColor(R.color.dark_orange),
                getResources().getColor(R.color.dark_orange)
        );

        return rootView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            ServiceResponse s = args.getParcelable(AppConst.PARC_SERVICE_RESP);

            if (s != null) {

                if (s.isSuccess()) {

                    ServiceModel service = s.getServiceModel();

                    if (service.isSuccess()) {

                        // Kuota Progress bar
                        List<Bucket> buckets = service.getBuckets();
                        if (buckets.size() > 0) {

                            LayoutInflater inflater = (LayoutInflater) getActivity()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                            for (Bucket b : buckets) {

                                View kuotaLayout = inflater
                                        .inflate(R.layout.kuota_progress_layout, null);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.setMargins(0, 0, 0, 10);
                                kuotaLayout.setLayoutParams(layoutParams);

                                TextView tvProgressLabel =
                                        (TextView) kuotaLayout.findViewById(R.id.label_progress_kuota);
                                TextProgressBar textProgressBar =
                                        (TextProgressBar) kuotaLayout.findViewById(R.id.progress_kuota);

                                tvProgressLabel.setText(b.getLabel());

                                if (!service.isQuotaExceeded()) {
                                    textProgressBar.setText(b.getRemainingQuotaFmt());

                                    int maxProgressBar = b.getMaxBucketSize() * 10;
                                    int progressProgressBar = (int) (Double.valueOf(b.getCurrentBucketSize()) * 10);

                                    textProgressBar.setMax(maxProgressBar);
                                    textProgressBar.setProgress(progressProgressBar);

                                } else {
                                    textProgressBar.setProgress(0);
                                }

                                linearLayoutKuotaProgressbar.addView(kuotaLayout);
                            }
                        }

                        // deskripsi paket
                        descPacket.setText(getString(R.string.text_desc_packet) + " " + s.getMemberPacket());

                        // masa aktif, upload dan download speed
                        if (!service.isActiveTimeExceeded()) {
                            descActive.setText(
                                    getString(R.string.text_desc_active) + " "
                                            + service.getDateEnd() + " "
                                            + getString(R.string.pukul) + " "
                                            + service.getTimeEnd()
                            );

                            descSpeed.setText(getString(R.string.text_desc_speed)
                                    + " " + service.getDownloadSpeed() + " / " + service.getUploadSpeed());

                        } else {
                            descActive.setText(getString(R.string.active_time_exceeded));
                            descSpeed.setText(getString(R.string.nonactiveted_service));
                        }

                    } else { // Service stat return NO Data

                        showNoDataAlert();
                    }

                } else {
                    descPacket.setText(getString(R.string.in_active_service));
                    descActive.setText(getString(R.string.in_active_service));
                    descSpeed.setText(getString(R.string.in_active_service));
                }

            }

            // Populate user info
            user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvFullname.setText(user.getFullname());
                tvCustId.setText(Utility.getCustIdPrf(getActivity()));
                tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));
            }
        }
    }

    @Override
    public void onRefresh() {
        mCallback.onSwipeListener();
        serviceSwipeRefreshLayout.setRefreshing(false);
    }

    @OnClick(R.id.humb_service) public void clickHumbService(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.serviceFragmentClicListener(args);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ServiceFragmentInf) context;
        } catch (ClassCastException c) {
            throw new ClassCastException(context + " activity must implement ServiceFragmentInf.");
        }
    }

    private void showNoDataAlert() {
        android.support.v7.app.AlertDialog.Builder builder =
                new android.support.v7.app.AlertDialog.Builder(getActivity());

        LayoutInflater layoutInflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = layoutInflater.inflate(R.layout.custom_alert_dialog, null);
        TextView tvCustomAlert = (TextView) dialogView.findViewById(R.id.tv_custom_alert);
        tvCustomAlert.setText(getString(R.string.text_error_service_stat));

        builder
                .setView(dialogView)
                .setTitle(AppConst.TITLE_MANGOESKY)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.text_reload),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                                mCallback.onSwipeListener();
                            }
                        })
                .setNegativeButton(getString(R.string.text_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                .show();
    }
}
