package com.mangoesky.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.ImageFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.NotificationsAdapter;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.DatabaseHandler;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.MyNotif;
import com.mangoesky.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private List<MyNotif> notifList = new ArrayList<>();
    private String custId;
    private DatabaseHandler db;
    private final String LOG_TAG = this.getClass().getSimpleName();
    private NotificationsAdapter notificationsAdapter;
    @BindView(R.id.recycler_notifications)
    RecyclerView recyclerNotifications;
    @BindView(R.id.tv_no_notifs)
    TextView tvNoNotifs;
    @BindView(R.id.user_info_custid)
    TextView tvCusId;
    @BindView(R.id.user_info_fullname)
    TextView tvFullname;
    @BindView(R.id.user_info_layout)
    LinearLayout linearLayoutUserInfo;
    @BindView(R.id.user_info_title)
    TextView tvTitle;

    private NotificationFragmentInf mCallback;


    public NotificationFragment() {
        // Required empty public constructor
    }

    public interface NotificationFragmentInf {
        void NotificationFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);

        ButterKnife.bind(this, rootView);

        linearLayoutUserInfo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.dark_orange));

        Bundle args = getArguments();
        if (args != null) {
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvCusId.setText(Utility.getCustIdPrf(getActivity()));
                tvFullname.setText(user.getFullname());
            }

            // Screen Title
            tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        custId = Utility.getCustIdPrf(getActivity());

        db = new DatabaseHandler(getActivity());

        setUpRecyclerView();
        readNotifData();

    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(AppConst.FILTER_STRING));
        readNotifData();
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (NotificationFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " must implement NotificationFragmentInf");
        }
    }

    @OnClick(R.id.humb_notification) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.NotificationFragmentClickListener(args);
    }

    private void readNotifData() {

        final List<MyNotif> notifListFromDb = db.getAllNotif(custId);

        if (!notifListFromDb.isEmpty()) {
            recyclerNotifications.setVisibility(View.VISIBLE);
            tvNoNotifs.setVisibility(View.GONE);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifList.clear();
                    for (MyNotif m : notifListFromDb) {
                        notifList.add(new MyNotif(
                                m.getNotifTitle(),
                                m.getNotifBody(),
                                m.getIsRead(),
                                m.getRecievedAT(),
                                m.getCustomer_id()
                        ));
                    }

                    notificationsAdapter.notifyDataSetChanged();
                }
            });

        } else {

            recyclerNotifications.setVisibility(View.GONE);
            tvNoNotifs.setVisibility(View.VISIBLE);
        }
    }

    private void setUpRecyclerView() {
        notificationsAdapter = new NotificationsAdapter(getActivity(), notifList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerNotifications.setLayoutManager(layoutManager);
        recyclerNotifications.setItemAnimator(new DefaultItemAnimator());
        recyclerNotifications.setAdapter(notificationsAdapter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            readNotifData();
        }
    };

}
