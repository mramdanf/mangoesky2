package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {

    private ChangePasswordFragmentInf mCallback;
    @BindView(R.id.cp_old_password)
    EditText etOldPass;
    @BindView(R.id.cp_new_password)
    EditText etNewPass;
    @BindView(R.id.confirm_new_password)
    EditText etConfirPass;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    public interface ChangePasswordFragmentInf {
        void ChangePasswordClickListerner(int itemId, Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this, rootView);
        getActivity().setTitle(getString(R.string.change_password));
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (ChangePasswordFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implement ChangePasswordFragmentInf");
        }
    }

    @OnClick(R.id.btn_cp_back) public void cpGoBackButton(View v) {
        mCallback.ChangePasswordClickListerner(v.getId(), null);
    }

    @OnClick(R.id.btn_cp_simpan) public void cpSimpanNewPassword(View v) {
        String oldPass = etOldPass.getText().toString();
        String newPass = etNewPass.getText().toString();
        String confirmPass = etConfirPass.getText().toString();

        if (!oldPass.isEmpty() && !newPass.isEmpty() && !confirmPass.isEmpty()) {

            if (newPass.equals(confirmPass)) {
                Bundle args = new Bundle();
                args.putString(AppConst.OLD_PASSWORD, oldPass);
                args.putString(AppConst.NEW_PASSWORD, newPass );
                args.putString(AppConst.CONFIRM_NEW_PASSWORD, confirmPass);

                mCallback.ChangePasswordClickListerner(v.getId(), args);

            } else {
                Utility.displayAlert(getActivity(), getString(R.string.error_confrim_password));
            }

        } else {
            Utility.displayAlert(getActivity(), getString(R.string.empty_field));
        }

    }
}
