package com.mangoesky.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.TopupListAdapter;
import com.mangoesky.apiresponsewrapper.TopupPacketResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.RecyclerItemClickListener;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.TopupPacketModel;
import com.mangoesky.model.User;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopupListFragment extends Fragment implements
        RecyclerItemClickListener.OnItemClickListener{

    @BindView(R.id.recycler_topup)
    RecyclerView recyclerTopup;
    @BindView(R.id.user_info_custid)
    TextView tvCustId;
    @BindView(R.id.user_info_fullname)
    TextView tvFullname;
    @BindView(R.id.user_info_title)
    TextView tvTitle;
    private List<TopupPacketModel> topupPacketModelList;
    private TopupListAdapter adapter;
    private final String LOG_TAG = this.getClass().getSimpleName();
    private TopupPacketModel topupPacketModel;
    private TopupListFragmentInf mCallback;



    public TopupListFragment() {
        // Required empty public constructor
    }

    public interface TopupListFragmentInf {
        void topupListFragmentClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_topup_list, container, false);
        ButterKnife.bind(this, rootView);

        Bundle args = getArguments();
        if (args != null) {
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvCustId.setText(Utility.getCustIdPrf(getActivity()));
                tvFullname.setText(user.getFullname());
            }

            // Screen Title
            tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));
        }

        setupRecyclerView();

        prepareTopupListData();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onItemClick(View childView, int position) {
        if (topupPacketModelList != null) {
            topupPacketModel = topupPacketModelList.get(position);
            if (topupPacketModel != null) showTopupViewChoice();
        }
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (TopupListFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implement TopupListFragmentInf");
        }
    }

    @OnClick(R.id.humb_topup_list) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.topupListFragmentClickListener(args);
    }

    private void showTopupViewChoice() {
        CharSequence[] topupViewString = {"blanja.com", "elevenia.co.id", "Finpay"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(getString(R.string.buy_in))
                .setItems(topupViewString, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String url = "";
                        Intent i;
                        switch (which) {
                            case 0:
                                url = topupPacketModel.getBlanjaItemUrl();
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                break;
                            case 1:
                                url = AppConst.ElEVANIA_URL + topupPacketModel.getElevaniaItemId();
                                i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                break;
                            case 2:
                                Bundle args = new Bundle();
                                args.putParcelable(AppConst.PARC_PACKET, topupPacketModel);
                                args.putString(AppConst.TAG_PAYMENT_METHODE, getString(R.string.text_direct_finpay));
                                args.putInt(AppConst.ITEM_ID, AppConst.FINPAY_ACTION_TAG);
                                mCallback.topupListFragmentClickListener(args);
                                break;
                            default:
                                break;

                        }

                    }
                }).show();
    }

    private void prepareTopupListData() {
        Bundle args = getArguments();
        if (args != null) {
            TopupPacketResponse t = args.getParcelable(AppConst.PARC_TOPUP_PACKET_RESP);
            if (t != null) {
                if (t.getTopupPacketModelList() != null) {
                    topupPacketModelList = t.getTopupPacketModelList();
                    adapter = new TopupListAdapter(getActivity(), t.getTopupPacketModelList());
                    recyclerTopup.setAdapter(adapter);
                } else {
                    Log.d(LOG_TAG, "list was null");
                }

            } else {
                Log.d(LOG_TAG, "t was null");
            }

        } else {
            Log.d(LOG_TAG, "args null");
        }
    }

    private void setupRecyclerView() {

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerTopup.setLayoutManager(layoutManager);
        recyclerTopup.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerTopup.setItemAnimator(new DefaultItemAnimator());
        recyclerTopup.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), this));
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
