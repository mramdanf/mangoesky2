package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {

    private ArrayAdapter adapter;
    @BindView(R.id.edit_gender)
    Spinner spinnerGender;
    @BindView(R.id.edit_fullname)
    EditText editFullname;
    @BindView(R.id.edit_email)
    EditText editEmail;
    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.edit_address)
    EditText editAddress;
    private EditProfileInf mCallback;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    public interface EditProfileInf {
        void EditProfileClickListener(int itemId, Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this, rootView);
        getActivity().setTitle(getString(R.string.edit_profile));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String [] gender = {"M", "F"};
        adapter = new ArrayAdapter(getActivity(),  android.R.layout.simple_spinner_item, gender);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapter);

        Bundle args = getArguments();
        if (args != null) {
            User u = args.getParcelable(AppConst.PARC_USER);
            if (u != null) {
                editFullname.setText(u.getFullname());
                editEmail.setText(u.getEmail());
                editPhone.setText(u.getPhone());
                editAddress.setText(u.getAddress());

                spinnerGender.setSelection(adapter.getPosition(u.getGender()));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (EditProfileInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implemnt EditProfileInf.");
        }
    }

    @OnClick(R.id.btn_edit_back) public void editGoBack(View v) {
        mCallback.EditProfileClickListener(v.getId(), null);
    }

    @OnClick(R.id.btn_edit_simpan) public void saveProfileChange(View v) {
        User u = new User();
        u.setFullname(editFullname.getText().toString());
        u.setEmail(editEmail.getText().toString());
        u.setPhone(editPhone.getText().toString());
        u.setGender(spinnerGender.getSelectedItem().toString());
        u.setAddress(editAddress.getText().toString());

        Bundle args = new Bundle();
        args.putParcelable(AppConst.PARC_USER, u);

        mCallback.EditProfileClickListener(v.getId(), args);
    }
}
