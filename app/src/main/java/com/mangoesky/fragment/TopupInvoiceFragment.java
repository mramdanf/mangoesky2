package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopupInvoiceFragment extends Fragment {

    @BindView(R.id.order_type) TextView orderType;
    @BindView(R.id.invoice) TextView invoice;
    @BindView(R.id.order_price) TextView orderPrice;
    @BindView(R.id.order_code) TextView orderCode;
    @BindView(R.id.order_date_time) TextView orderDateTime;

    @BindView(R.id.btn_finish_order)
    Button btnFinishOrder;

    private TopupInvoiceFragmentInf mCallback;


    public TopupInvoiceFragment() {
        // Required empty public constructor
    }

    public interface TopupInvoiceFragmentInf {
        void topupInvoiceClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_topup_invoice, container, false);

        getActivity().setTitle(getString(R.string.invoice_text));

        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if(args != null){
            orderType.setText(
                    Html.fromHtml("Order Pembelian <b> Topup Mangoesky "
                            + args.getString(AppConst.TAG_NAME) + "</b>")
            );
            invoice.setText(
                    Html.fromHtml("Nomor invoice Anda <b>"
                            + args.getString(AppConst.TAG_INVOICE) +"</b>")
            );
            orderPrice.setText(
                    Html.fromHtml("Silahkan lakukan pembayaran sebesar <b>Rp. "
                            + String.format(Locale.US, "%,d", Integer.parseInt(args.getString(AppConst.TAG_PRICE)))
                            + "</b> dengan kode pembayaran")
            );
            orderCode.setText(args.getString(AppConst.TAG_CODE));
            orderDateTime.setText(
                    Html.fromHtml("sebelum tanggal <b>"
                            + args.getString(AppConst.TAG_DATE)
                            + "</b> pukul <b>" + args.getString(AppConst.TAG_TIME)
                            + "</b>")
            );

            if (args.getString(AppConst.TAG_FLAG) != null &&
                    args.getString(AppConst.TAG_FLAG).equals("ControllerActivity")) {
                btnFinishOrder.setText(getString(R.string.text_back));
            }

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (TopupInvoiceFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " Activity must implement TopupInvoiceFragmentInf");
        }
    }

    @OnClick(R.id.btn_finish_order)
    public void finishOrder(){
        Bundle args = new Bundle();
        args.putInt(AppConst.TOPUPINV_CLICK_ID, R.id.btn_finish_order);
        mCallback.topupInvoiceClickListener(args);
    }

    @OnClick(R.id.copy_pc)
    public void copyPc() {
        Bundle args = new Bundle();
        args.putInt(AppConst.TOPUPINV_CLICK_ID, R.id.copy_pc);
        args.putString(AppConst.TAG_CODE, orderCode.getText().toString());
        mCallback.topupInvoiceClickListener(args);
    }

    @OnClick(R.id.btn_how_topay)
    public void howToPay() {
        Bundle args = new Bundle();
        args.putInt(AppConst.TOPUPINV_CLICK_ID, R.id.btn_how_topay);
        mCallback.topupInvoiceClickListener(args);
    }

    @OnClick(R.id.btn_list_topup)
    public void listTopup() {
        Bundle args = new Bundle();
        args.putInt(AppConst.TOPUPINV_CLICK_ID, R.id.btn_list_topup);
        mCallback.topupInvoiceClickListener(args);
    }
}
