package com.mangoesky.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.TopUpAdapter;
import com.mangoesky.apiresponsewrapper.HistoryResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.model.LogTopup;
import com.mangoesky.model.Topup;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopupFragment extends Fragment {

    ArrayList topupArrayList = new ArrayList();
    ListView topupHistory;


    public TopupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_topup, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        topupHistory = (ListView)view.findViewById(R.id.topup_listView2);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date_topup);

        Bundle args = getArguments();
        if (args != null) {
            tvDate.setText(args.getString(AppConst.SELECTED_DATE));
            HistoryResponse h = args.getParcelable(AppConst.PARC_HISTORY_RESP);
            prepareTopupData(h);
        }

    }

    private void prepareTopupData(HistoryResponse h) {
        List<LogTopup> logTopupList = h.getLogTopupList();

        topupArrayList.clear();
        for(int i = 0; i < logTopupList.size(); i++) {
            LogTopup topupRow = logTopupList.get(i);
            Topup data = new Topup();
            data.setNumber((i));
            data.setAmount(topupRow.getTopupAmount());
            data.setDatetime(topupRow.getTopupDatetime());
            topupArrayList.add(data);
        }
        topupHistory.setAdapter(new TopUpAdapter(getActivity().getApplicationContext(), topupArrayList));
    }
}
