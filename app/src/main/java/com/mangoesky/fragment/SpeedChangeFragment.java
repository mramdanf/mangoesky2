package com.mangoesky.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.adapter.SpeedAdapter;
import com.mangoesky.apiresponsewrapper.HistoryResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.model.LogSpeedChange;
import com.mangoesky.model.Speed;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpeedChangeFragment extends Fragment {

    ListView speedHistory;
    ArrayList speedArrayList = new ArrayList();


    public SpeedChangeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_speed_change, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        speedHistory = (ListView)view.findViewById(R.id.speed_listView2);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date_sc);

        Bundle args = getArguments();
        if (args != null) {
            tvDate.setText(args.getString(AppConst.SELECTED_DATE));
            HistoryResponse h = args.getParcelable(AppConst.PARC_HISTORY_RESP);
            prepareSCData(h);
        }
    }

    private void prepareSCData(HistoryResponse h) {
        List<LogSpeedChange> logSpeedChangeList = h.getLogSpeedChangeList();

        speedArrayList.clear();
        for(int i = 0; i < logSpeedChangeList.size(); i++) {
            LogSpeedChange logSpeedChange = logSpeedChangeList.get(i);
            String desc = logSpeedChange.getDesc();

            Speed data = new Speed(i, logSpeedChange.getDownloadSpeed() + " / "
                    + logSpeedChange.getUploadSpeed(), logSpeedChange.getDatetimeChange(), desc);

            speedArrayList.add(data);
        }
        speedHistory.setAdapter(new SpeedAdapter(getActivity().getApplicationContext(), speedArrayList));
    }
}
