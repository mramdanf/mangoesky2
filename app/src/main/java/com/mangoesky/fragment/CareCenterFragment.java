package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.apiresponsewrapper.CCResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.CustomerService;
import com.mangoesky.model.User;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CareCenterFragment extends Fragment {

    private final String LOG_TAG = this.getClass().getSimpleName();

    @BindView(R.id.user_info_custid) TextView tvCustId;
    @BindView(R.id.user_info_fullname) TextView tvFullname;
    @BindView(R.id.user_info_layout) LinearLayout linearLayoutUserInfo;
    @BindView(R.id.user_info_title) TextView tvTitle;
    @BindView(R.id.care_center_header) ImageView imageViewCareCenterHeader;

    @BindView(R.id.wrapper_cc_email) LinearLayout wrapperCCEmail;
    @BindView(R.id.wrapper_cc_phone) LinearLayout wrapperCCPhone;
    @BindView(R.id.wrapper_cc_address) LinearLayout wrapperCCAddress;
    @BindView(R.id.wrapper_cc_website) LinearLayout wrapperCCWebsite;

    private CareCenterClickListener mCallback;

    public CareCenterFragment() {
        // Required empty public constructor
    }

    public interface CareCenterClickListener{
        void CareCenterClickListener(Bundle args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_care_center, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            CCResponse ccResponse = args.getParcelable(AppConst.PARC_CC_RESP);

            if (ccResponse != null) {
                CustomerService cc = ccResponse.getCustomerService();

                if (cc != null) {

                    LayoutInflater inflater = getActivity().getLayoutInflater();

                    // Email
                    for (int i = 0; i < cc.getEmail().size(); i++) {
                        TextView subContentEmail =
                                (TextView) inflater.inflate(R.layout.carecenter_subcontent, null);
                        subContentEmail.setText(cc.getEmail().get(i));
                        wrapperCCEmail.addView(subContentEmail);
                    }

                    // Phone
                    for (int i = 0; i < cc.getPhone().size(); i++) {
                        TextView subContentPhone =
                                (TextView) inflater.inflate(R.layout.carecenter_subcontent, null);
                        subContentPhone.setText(cc.getPhone().get(i));
                        wrapperCCPhone.addView(subContentPhone);
                    }

                    // Address
                    for (int i = 0; i < cc.getAddress().size(); i++) {
                        TextView subContentAddress =
                                (TextView) inflater.inflate(R.layout.carecenter_subcontent, null);
                        subContentAddress.setText(cc.getAddress().get(i));
                        wrapperCCAddress.addView(subContentAddress);
                    }

                    // Website
                    for (int i = 0; i < cc.getWebsite().size(); i++) {
                        TextView subContentAddress =
                                (TextView) inflater.inflate(R.layout.carecenter_subcontent, null);
                        subContentAddress.setText(cc.getWebsite().get(i));
                        wrapperCCWebsite.addView(subContentAddress);
                    }
                }
            }

            // Populate user info
            linearLayoutUserInfo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.dark_orange));
            User user = args.getParcelable(AppConst.PARC_USER);
            if (user != null) {
                tvCustId.setText(Utility.getCustIdPrf(getActivity()));
                tvFullname.setText(user.getFullname());
            }

            // Screen title
            tvTitle.setText(args.getString(AppConst.TAG_TITLE_USER_INFO));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (CareCenterClickListener) context;
        } catch (ClassCastException e) {
            throw  new ClassCastException(context + " must implement CareCenterClickListener");
        }
    }

    @OnClick(R.id.humb_care_center) public void showNavigationDrawer(View v) {
        Bundle args = new Bundle();
        args.putInt(AppConst.ITEM_ID, v.getId());
        mCallback.CareCenterClickListener(args);
    }
}
