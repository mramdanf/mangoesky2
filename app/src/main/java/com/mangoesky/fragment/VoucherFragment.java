package com.mangoesky.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mangoesky.R;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class VoucherFragment extends Fragment {

    @BindView(R.id.voucher_et_kode)
    TextView etKodeVoucher;

    private VoucherFragmentInf mCallback;


    public VoucherFragment() {
        // Required empty public constructor
    }

    public interface VoucherFragmentInf {
        void VoucherFragmentOnClickListener(int itemId, Bundle args, TextView tvVoucherCode);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_voucher, container, false);
        ButterKnife.bind(this, rootView);

        getActivity().setTitle(getString(R.string.title_activity_voucher));
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (VoucherFragmentInf) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " activity must implement VoucherFragmentInf.");
        }
    }

    @OnClick(R.id.voucher_btn_submit) public void submitVoucher(View v) {
        String vouhcerCode = etKodeVoucher.getText().toString();

        if (!vouhcerCode.isEmpty()) {
            Bundle args = new Bundle();
            args.putString(AppConst.POST_VOUCHER_CODE, vouhcerCode);
            mCallback.VoucherFragmentOnClickListener(v.getId(), args, etKodeVoucher);

        } else {
            Utility.displayAlert(getActivity(), getString(R.string.error_empty_vouchercode));
        }


    }

    @OnClick(R.id.humb_voucher) public void showNavigationDrawer(View v) {
        mCallback.VoucherFragmentOnClickListener(v.getId(), null, null);
    }
}
