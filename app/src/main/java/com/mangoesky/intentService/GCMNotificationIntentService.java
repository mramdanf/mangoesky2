package com.mangoesky.intentService;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mangoesky.R;
import com.mangoesky.activity.ControllerActivity;
import com.mangoesky.activity.SplashActivity;
import com.mangoesky.helper.AppConst;
import com.mangoesky.helper.DatabaseHandler;
import com.mangoesky.helper.GcmBroadcastReceiver;
import com.mangoesky.helper.Utility;
import com.mangoesky.model.MyNotif;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GCMNotificationIntentService extends IntentService {
	// Sets an ID for the notification, so it can be updated
	public static final int notifyID = 9002;
	NotificationCompat.Builder builder;
	public static final String LOG_TAG = GCMNotificationIntentService.class.getSimpleName();

	String notifTitle;

	String dataAlert[] = new String[2];

	public GCMNotificationIntentService() {
		super("GcmIntentService");
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(), null);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "
						+ extras.toString(), null);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				dataAlert[0] = String.valueOf(extras.get(AppConst.MSG_KEY));
				dataAlert[1] = String.valueOf(extras.get(AppConst.MSG_TITLE));

				sendNotification(String.valueOf(extras.get(AppConst.MSG_KEY)), dataAlert);
			}

		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg, String[] alertData) {
		Intent resultIntent = new Intent(this, SplashActivity.class);
		resultIntent.putExtra(AppConst.FROM_NOTIF, "true");
		resultIntent.putExtra(AppConst.MSG, msg);
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
				resultIntent, PendingIntent.FLAG_ONE_SHOT);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder = new Notification.Builder(this);
		builder.setContentTitle("Mangoesky")
				.setContentText("You have new message")
				.setSmallIcon(R.mipmap.ic_launcher)
				.setAutoCancel(true);

		// Set pending intent
		builder.setContentIntent(resultPendingIntent);

		// Set Vibrate, Sound and Light
		int defaults = 0;
		defaults = defaults | Notification.DEFAULT_LIGHTS;
		defaults = defaults | Notification.DEFAULT_VIBRATE;
		defaults = defaults | Notification.DEFAULT_SOUND;

		builder.setDefaults(defaults);

		Notification notification = new Notification.BigTextStyle(builder)
				.bigText(msg).build();

		notificationManager.notify(notifyID, notification);

        //flag untuk menjalankan auto refresh
        SharedPreferences setFlagToUpdateInbox = getSharedPreferences(AppConst.MANGO_DATA, MODE_PRIVATE);
        SharedPreferences.Editor editor = setFlagToUpdateInbox.edit();
        editor.putBoolean("isNewInbox", true);
        editor.apply();

        //inserting notification to sqlite
		DatabaseHandler db = new DatabaseHandler(this);
		String cust_id = Utility.getCustIdPrf(this);

		String msgBody = alertData[0];
		String msgTitle = alertData[1];

		db.addNotif(new MyNotif(msgTitle, msgBody, "no", getDateTime(), cust_id));

		// broadcasting to local broadcast manager
		LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(AppConst.FILTER_STRING));

	}

	private String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}
}
