package com.mangoesky.apiendpoint;

import com.mangoesky.apiresponsewrapper.BuyTopupResponse;
import com.mangoesky.apiresponsewrapper.CCResponse;
import com.mangoesky.apiresponsewrapper.DetailTHResponse;
import com.mangoesky.apiresponsewrapper.HistoryResponse;
import com.mangoesky.apiresponsewrapper.IntroSlideResponse;
import com.mangoesky.apiresponsewrapper.LoginResponse;
import com.mangoesky.apiresponsewrapper.ProfileResponse;
import com.mangoesky.apiresponsewrapper.ServiceResponse;
import com.mangoesky.apiresponsewrapper.TopupHistoryResponse;
import com.mangoesky.apiresponsewrapper.TopupPacketResponse;
import com.mangoesky.helper.AppConst;
import com.mangoesky.model.CareCenter;
import com.mangoesky.model.SimpleModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Ramdan Firdaus on 9/2/2017.
 */

public interface ApiEndPoint {

    @POST("change_password")
    @FormUrlEncoded
    Call<SimpleModel> changePasswordApi(
            @Field(AppConst.OLD_PASSWORD) String oldPass,
            @Field(AppConst.NEW_PASSWORD) String newPass,
            @Field(AppConst.CONFIRM_NEW_PASSWORD) String confirNewPass
    );

    @POST("logout")
    @FormUrlEncoded
    Call<SimpleModel> logoutApi(
            @Field(AppConst.POST_REGID) String regId
    );

    @GET("customer_service_dev")
    Call<CCResponse> getCCApi();

    @POST("submit_topup")
    @FormUrlEncoded
    Call<SimpleModel> submitVoucherApi(
            @Field("voucher_code") String voucherCode
    );

    @GET("dashboard")
    Call<ServiceResponse> getServiceApi();

    @POST("update_profile")
    @FormUrlEncoded
    Call<SimpleModel> updateProfile(
            @Field(AppConst.POST_FULLNAME) String fullname,
            @Field(AppConst.POST_EMAIL) String custEmal,
            @Field(AppConst.POST_ADDRESS) String custAdd,
            @Field(AppConst.POST_PHONE) String custPhone,
            @Field(AppConst.POST_GENDER) String custGender
    );

    @GET("transaction_history")
    Call<HistoryResponse> getHistory(
            @Query("month") int month,
            @Query("year") int year
    );

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> authCheck(
            @Field(AppConst.POST_REGID) String regId
    );

    @GET("profile")
    Call<ProfileResponse> getPofileApi();

    @GET("topup_packet")
    Call<TopupPacketResponse> getListTopupPacket();

    @FormUrlEncoded
    @POST("topup_finpay")
    Call<BuyTopupResponse> goForPaidTopupApi(
            @Field(AppConst.TAG_TOPUP_PACKET_ID) String topupPacketId
    );

    @GET("topup_order")
    Call<TopupHistoryResponse> getTopupHistory();

    @GET
    Call<DetailTHResponse> getDetailTH(@Url String url);

    @GET("intro_slides")
    Call<IntroSlideResponse> getIntroSlides();

    /* ----- dummy api ----- */
    @GET("58bd218d0f00005e165c668a")
    Call<LoginResponse> getDummyLogin();
    /* ----- end of dummy api ----- */


}
