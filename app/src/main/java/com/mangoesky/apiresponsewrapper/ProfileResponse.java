package com.mangoesky.apiresponsewrapper;

import com.mangoesky.model.User;

/**
 * Created by root on 2/3/17.
 */

public class ProfileResponse {
    boolean success;
    User user;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
