package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.LogSpeedChange;
import com.mangoesky.model.LogTopup;
import com.mangoesky.model.PacketSubscr;
import com.mangoesky.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ramdan Firdaus on 6/2/2017.
 */

public class HistoryResponse implements Parcelable {

    @SerializedName("packet_subscr")
    List<PacketSubscr> packetSubscrList;
    @SerializedName("log_topup")
    List<LogTopup> logTopupList;
    @SerializedName("log_speed_change")
    List<LogSpeedChange> logSpeedChangeList;
    User user;

    public List<PacketSubscr> getPacketSubscrList() {
        return packetSubscrList;
    }

    public void setPacketSubscrList(List<PacketSubscr> packetSubscrList) {
        this.packetSubscrList = packetSubscrList;
    }

    public List<LogTopup> getLogTopupList() {
        return logTopupList;
    }

    public void setLogTopupList(List<LogTopup> logTopupList) {
        this.logTopupList = logTopupList;
    }

    public List<LogSpeedChange> getLogSpeedChangeList() {
        return logSpeedChangeList;
    }

    public void setLogSpeedChangeList(List<LogSpeedChange> logSpeedChangeList) {
        this.logSpeedChangeList = logSpeedChangeList;
    }

    public HistoryResponse() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.packetSubscrList);
        dest.writeList(this.logTopupList);
        dest.writeTypedList(this.logSpeedChangeList);
        dest.writeParcelable(this.user, flags);
    }

    protected HistoryResponse(Parcel in) {
        this.packetSubscrList = in.createTypedArrayList(PacketSubscr.CREATOR);
        this.logTopupList = new ArrayList<LogTopup>();
        in.readList(this.logTopupList, LogTopup.class.getClassLoader());
        this.logSpeedChangeList = in.createTypedArrayList(LogSpeedChange.CREATOR);
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<HistoryResponse> CREATOR = new Creator<HistoryResponse>() {
        @Override
        public HistoryResponse createFromParcel(Parcel source) {
            return new HistoryResponse(source);
        }

        @Override
        public HistoryResponse[] newArray(int size) {
            return new HistoryResponse[size];
        }
    };
}
