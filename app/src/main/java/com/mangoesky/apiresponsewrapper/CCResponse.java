package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.CareCenter;
import com.mangoesky.model.CustomerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ramdan Firdaus on 10/2/2017.
 */

public class CCResponse implements Parcelable {

    @SerializedName("customer_service")
    private CustomerService customerService;

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.customerService, flags);
    }

    public CCResponse() {
    }

    protected CCResponse(Parcel in) {
        this.customerService = in.readParcelable(CustomerService.class.getClassLoader());
    }

    public static final Creator<CCResponse> CREATOR = new Creator<CCResponse>() {
        @Override
        public CCResponse createFromParcel(Parcel source) {
            return new CCResponse(source);
        }

        @Override
        public CCResponse[] newArray(int size) {
            return new CCResponse[size];
        }
    };
}
