package com.mangoesky.apiresponsewrapper;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.Payment;

/**
 * Created by Ramdan Firdaus on 4/11/2016.
 */
public class BuyTopupResponse {
    boolean success;
    String msg;
    @SerializedName("topup_order")
    Payment payment;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
