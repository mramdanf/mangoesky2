package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.ServiceModel;

/**
 * Created by Ramdan Firdaus on 10/2/2017.
 */
/*
* {
  "success": true,
  "fullname": "Asrafin Danang",
  "customer_id": "310000000000",
  "member_packet": "Paket TRIAL Internet Broadband",
  "service_stat": {
*
* */
public class ServiceResponse implements Parcelable {
    boolean success;
    @SerializedName("member_packet")
    String memberPacket;
    @SerializedName("service_stat")
    ServiceModel serviceModel;
    String fullname;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMemberPacket() {
        return memberPacket;
    }

    public void setMemberPacket(String memberPacket) {
        this.memberPacket = memberPacket;
    }

    public ServiceModel getServiceModel() {
        return serviceModel;
    }

    public void setServiceModel(ServiceModel serviceModel) {
        this.serviceModel = serviceModel;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.memberPacket);
        dest.writeParcelable(this.serviceModel, flags);
        dest.writeString(this.fullname);
    }

    public ServiceResponse() {
    }

    protected ServiceResponse(Parcel in) {
        this.success = in.readByte() != 0;
        this.memberPacket = in.readString();
        this.serviceModel = in.readParcelable(ServiceModel.class.getClassLoader());
        this.fullname = in.readString();
    }

    public static final Creator<ServiceResponse> CREATOR = new Creator<ServiceResponse>() {
        @Override
        public ServiceResponse createFromParcel(Parcel source) {
            return new ServiceResponse(source);
        }

        @Override
        public ServiceResponse[] newArray(int size) {
            return new ServiceResponse[size];
        }
    };
}
