package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.TopupPacketModel;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 7/2/2017.
 */

public class TopupPacketResponse implements Parcelable {
    boolean success;

    @SerializedName("topup_packet")
    List<TopupPacketModel> topupPacketModelList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<TopupPacketModel> getTopupPacketModelList() {
        return topupPacketModelList;
    }

    public void setTopupPacketModelList(List<TopupPacketModel> topupPacketModelList) {
        this.topupPacketModelList = topupPacketModelList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.topupPacketModelList);
    }

    public TopupPacketResponse() {
    }

    protected TopupPacketResponse(Parcel in) {
        this.success = in.readByte() != 0;
        this.topupPacketModelList = in.createTypedArrayList(TopupPacketModel.CREATOR);
    }

    public static final Creator<TopupPacketResponse> CREATOR = new Creator<TopupPacketResponse>() {
        @Override
        public TopupPacketResponse createFromParcel(Parcel source) {
            return new TopupPacketResponse(source);
        }

        @Override
        public TopupPacketResponse[] newArray(int size) {
            return new TopupPacketResponse[size];
        }
    };
}
