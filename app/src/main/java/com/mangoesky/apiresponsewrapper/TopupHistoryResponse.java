package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.TopupHistory;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 8/11/2016.
 */
public class TopupHistoryResponse implements Parcelable {
    boolean success;
    String msg;

    public List<TopupHistory> getTopupHistory() {
        return topupHistory;
    }

    public void setTopupHistory(List<TopupHistory> topupHistory) {
        this.topupHistory = topupHistory;
    }

    @SerializedName("topup_orders")
    List<TopupHistory>  topupHistory;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.msg);
        dest.writeTypedList(this.topupHistory);
    }

    public TopupHistoryResponse() {
    }

    protected TopupHistoryResponse(Parcel in) {
        this.success = in.readByte() != 0;
        this.msg = in.readString();
        this.topupHistory = in.createTypedArrayList(TopupHistory.CREATOR);
    }

    public static final Creator<TopupHistoryResponse> CREATOR = new Creator<TopupHistoryResponse>() {
        @Override
        public TopupHistoryResponse createFromParcel(Parcel source) {
            return new TopupHistoryResponse(source);
        }

        @Override
        public TopupHistoryResponse[] newArray(int size) {
            return new TopupHistoryResponse[size];
        }
    };
}
