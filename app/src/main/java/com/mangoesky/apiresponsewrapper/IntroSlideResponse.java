package com.mangoesky.apiresponsewrapper;

/**
 * Created by Ramdan Firdaus on 21/4/2017.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.IntroSlide;

public class IntroSlideResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("intro_slides")
    @Expose
    private List<IntroSlide> introSlides = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<IntroSlide> getIntroSlides() {
        return introSlides;
    }

    public void setIntroSlides(List<IntroSlide> introSlides) {
        this.introSlides = introSlides;
    }
}
