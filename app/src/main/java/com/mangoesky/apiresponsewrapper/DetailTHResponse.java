package com.mangoesky.apiresponsewrapper;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.TopupHistory;

/**
 * Created by Ramdan Firdaus on 29/11/2016.
 */

public class DetailTHResponse {
    boolean success;
    String msg;
    @SerializedName("topup_order")
    TopupHistory topupHistory;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public TopupHistory getTopupHistory() {
        return topupHistory;
    }

    public void setTopupHistory(TopupHistory topupHistory) {
        this.topupHistory = topupHistory;
    }
}
