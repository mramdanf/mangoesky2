package com.mangoesky.apiresponsewrapper;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mangoesky.model.Banner;
import com.mangoesky.model.User;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 22/2/2017.
 */

public class LoginResponse implements Parcelable {
    boolean success;
    String msg;
    User user;

    public List<Banner> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<Banner> bannerList) {
        this.bannerList = bannerList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @SerializedName("banners")
    List<Banner> bannerList;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.msg);
        dest.writeParcelable(this.user, flags);
        dest.writeTypedList(this.bannerList);
    }

    public LoginResponse() {
    }

    protected LoginResponse(Parcel in) {
        this.success = in.readByte() != 0;
        this.msg = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.bannerList = in.createTypedArrayList(Banner.CREATOR);
    }

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel source) {
            return new LoginResponse(source);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
}
