package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 6/2/2017.
 */

public class LogSpeedChange implements Parcelable {
    @SerializedName("download_speed")
    String downloadSpeed;
    @SerializedName("upload_speed")
    String uploadSpeed;
    @SerializedName("datetime_change")
    String datetimeChange;
    String desc;

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public String getUploadSpeed() {
        return uploadSpeed;
    }

    public void setUploadSpeed(String uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public String getDatetimeChange() {
        return datetimeChange;
    }

    public void setDatetimeChange(String datetimeChange) {
        this.datetimeChange = datetimeChange;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public LogSpeedChange() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.downloadSpeed);
        dest.writeString(this.uploadSpeed);
        dest.writeString(this.datetimeChange);
        dest.writeString(this.desc);
    }

    protected LogSpeedChange(Parcel in) {
        this.downloadSpeed = in.readString();
        this.uploadSpeed = in.readString();
        this.datetimeChange = in.readString();
        this.desc = in.readString();
    }

    public static final Creator<LogSpeedChange> CREATOR = new Creator<LogSpeedChange>() {
        @Override
        public LogSpeedChange createFromParcel(Parcel source) {
            return new LogSpeedChange(source);
        }

        @Override
        public LogSpeedChange[] newArray(int size) {
            return new LogSpeedChange[size];
        }
    };
}
