package com.mangoesky.model;

/**
 * Created by Ramdan Firdaus on 31/1/2017.
 */

public class SimpleModel {
    String msg;
    boolean success;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
