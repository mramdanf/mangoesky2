package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 8/11/2016.
 */
public class TopupHistory implements Parcelable {
    String id;

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    @SerializedName("expiration_date")
    String expiration;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    String code;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTopupPacketName() {
        return topupPacketName;
    }

    public void setTopupPacketName(String topupPacketName) {
        this.topupPacketName = topupPacketName;
    }

    String invoice;
    String type;
    String price;
    String status;
    @SerializedName("topup_packet_name")
    String topupPacketName;
    @SerializedName("date_buy_fmt")
    String buyDateFmt;

    public String getBuyDateFmt() {
        return buyDateFmt;
    }

    public void setBuyDateFmt(String buyDateFmt) {
        this.buyDateFmt = buyDateFmt;
    }

    public String getTimeBuy() {
        return timeBuy;
    }

    public void setTimeBuy(String timeBuy) {
        this.timeBuy = timeBuy;
    }

    @SerializedName("time_buy")
    String timeBuy;

    /*=================== Parcelabel Implementations ======================== */
    protected TopupHistory(Parcel in) {
        invoice = in.readString();
        type = in.readString();
        price = in.readString();
        status = in.readString();
        topupPacketName = in.readString();
        code = in.readString();
        expiration = in.readString();
        timeBuy = in.readString();
        buyDateFmt = in.readString();
    }

    public static final Creator<TopupHistory> CREATOR = new Creator<TopupHistory>() {
        @Override
        public TopupHistory createFromParcel(Parcel parcel) {
            return new TopupHistory(parcel);
        }

        @Override
        public TopupHistory[] newArray(int i) {
            return new TopupHistory[i];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(invoice);
        parcel.writeString(type);
        parcel.writeString(price);
        parcel.writeString(status);
        parcel.writeString(topupPacketName);
        parcel.writeString(code);
        parcel.writeString(expiration);
        parcel.writeString(buyDateFmt);
        parcel.writeString(timeBuy);
    }
}
