package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/3/17.
 */
/*
"address":"Bogor",
        "gender":"M",
        "phone":"082113064268"*/
public class User implements Parcelable {
    String id;
    String email;
    @SerializedName("customer_id")
    String custId;
    String fullname;
    String address;
    String gender;
    String phone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.email);
        dest.writeString(this.custId);
        dest.writeString(this.fullname);
        dest.writeString(this.address);
        dest.writeString(this.gender);
        dest.writeString(this.phone);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.email = in.readString();
        this.custId = in.readString();
        this.fullname = in.readString();
        this.address = in.readString();
        this.gender = in.readString();
        this.phone = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
