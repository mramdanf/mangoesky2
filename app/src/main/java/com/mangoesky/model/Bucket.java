package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 8/5/2017.
 */

public class Bucket implements Parcelable {
    String label;
    @SerializedName("current_size")
    double currentBucketSize;
    @SerializedName("max_size")
    int maxBucketSize;
    @SerializedName("current_size_fmt")
    String remainingQuotaFmt;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getCurrentBucketSize() {
        return currentBucketSize;
    }

    public void setCurrentBucketSize(double currentBucketSize) {
        this.currentBucketSize = currentBucketSize;
    }

    public int getMaxBucketSize() {
        return maxBucketSize;
    }

    public void setMaxBucketSize(int maxBucketSize) {
        this.maxBucketSize = maxBucketSize;
    }

    public String getRemainingQuotaFmt() {
        return remainingQuotaFmt;
    }

    public void setRemainingQuotaFmt(String remainingQuotaFmt) {
        this.remainingQuotaFmt = remainingQuotaFmt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeDouble(this.currentBucketSize);
        dest.writeInt(this.maxBucketSize);
        dest.writeString(this.remainingQuotaFmt);
    }

    public Bucket() {
    }

    protected Bucket(Parcel in) {
        this.label = in.readString();
        this.currentBucketSize = in.readDouble();
        this.maxBucketSize = in.readInt();
        this.remainingQuotaFmt = in.readString();
    }

    public static final Parcelable.Creator<Bucket> CREATOR = new Parcelable.Creator<Bucket>() {
        @Override
        public Bucket createFromParcel(Parcel source) {
            return new Bucket(source);
        }

        @Override
        public Bucket[] newArray(int size) {
            return new Bucket[size];
        }
    };
}
