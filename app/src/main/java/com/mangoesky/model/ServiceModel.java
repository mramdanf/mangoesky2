package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 10/2/2017.
 */
/*
* "service_stat": {
    "success": true,
    "customer_name": "Asrafin Danang",
    "active_time_exceeded": false,
    "quota_exceeded": false,
    "datetime_end": 1488527202,
    "download_speed": "2 Mbps",
    "upload_speed": "512 Kbps",
    "current_speed": "up to 2 Mbps / 512 Kbps",
    "throttle_level": "0",
    "date_end": "3 Mar 2017",
    "time_end": "14:46:42",
    "remaining_quota": 2490107570,
    "remaining_quota_fmt": "2.49 GB"
  }
* */
public class ServiceModel implements Parcelable {

    boolean success;
    @SerializedName("download_speed")
    String downloadSpeed;
    @SerializedName("upload_speed")
    String uploadSpeed;
    @SerializedName("remaining_quota_fmt")
    String reaminingQuota;
    @SerializedName("date_end")
    String dateEnd;
    @SerializedName("time_end")
    String timeEnd;
    @SerializedName("quota_exceeded")
    boolean quotaExceeded;
    @SerializedName("active_time_exceeded")
    boolean activeTimeExceeded;
    @SerializedName("current_bucket_size")
    double currentBucketSize;
    @SerializedName("max_bucket_size")
    int maxBucketSize;
    List<Bucket> buckets;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public String getUploadSpeed() {
        return uploadSpeed;
    }

    public void setUploadSpeed(String uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public String getReaminingQuota() {
        return reaminingQuota;
    }

    public void setReaminingQuota(String reaminingQuota) {
        this.reaminingQuota = reaminingQuota;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public boolean isQuotaExceeded() {
        return quotaExceeded;
    }

    public void setQuotaExceeded(boolean quotaExceeded) {
        this.quotaExceeded = quotaExceeded;
    }

    public boolean isActiveTimeExceeded() {
        return activeTimeExceeded;
    }

    public void setActiveTimeExceeded(boolean activeTimeExceeded) {
        this.activeTimeExceeded = activeTimeExceeded;
    }

    public double getCurrentBucketSize() {
        return currentBucketSize;
    }

    public void setCurrentBucketSize(double currentBucketSize) {
        this.currentBucketSize = currentBucketSize;
    }

    public int getMaxBucketSize() {
        return maxBucketSize;
    }

    public void setMaxBucketSize(int maxBucketSize) {
        this.maxBucketSize = maxBucketSize;
    }

    public List<Bucket> getBuckets() {
        return buckets;
    }

    public void setBuckets(List<Bucket> buckets) {
        this.buckets = buckets;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.downloadSpeed);
        dest.writeString(this.uploadSpeed);
        dest.writeString(this.reaminingQuota);
        dest.writeString(this.dateEnd);
        dest.writeString(this.timeEnd);
        dest.writeByte(this.quotaExceeded ? (byte) 1 : (byte) 0);
        dest.writeByte(this.activeTimeExceeded ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.currentBucketSize);
        dest.writeInt(this.maxBucketSize);
        dest.writeTypedList(this.buckets);
    }

    public ServiceModel() {
    }

    protected ServiceModel(Parcel in) {
        this.success = in.readByte() != 0;
        this.downloadSpeed = in.readString();
        this.uploadSpeed = in.readString();
        this.reaminingQuota = in.readString();
        this.dateEnd = in.readString();
        this.timeEnd = in.readString();
        this.quotaExceeded = in.readByte() != 0;
        this.activeTimeExceeded = in.readByte() != 0;
        this.currentBucketSize = in.readDouble();
        this.maxBucketSize = in.readInt();
        this.buckets = in.createTypedArrayList(Bucket.CREATOR);
    }

    public static final Creator<ServiceModel> CREATOR = new Creator<ServiceModel>() {
        @Override
        public ServiceModel createFromParcel(Parcel source) {
            return new ServiceModel(source);
        }

        @Override
        public ServiceModel[] newArray(int size) {
            return new ServiceModel[size];
        }
    };
}
