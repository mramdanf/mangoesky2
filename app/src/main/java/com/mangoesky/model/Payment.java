package com.mangoesky.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 4/11/2016.
 */
public class Payment {
    String invoice;
    String code;
    @SerializedName("expiration_date")
    String expiration;

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }
}
