package com.mangoesky.model;

/**
 * Created by user on 20/3/2016.
 */
public class MyNotif {
    int id;
    String notifTitle;
    String notifBody;
    String isRead;
    String recievedAT;

    String customer_id;


    public MyNotif(String notifTitle, String notifBody,
                   String isRead, String recievedAT, String customer_id){
        this.notifTitle = notifTitle;
        this.notifBody = notifBody;
        this.isRead = isRead;
        this.recievedAT = recievedAT;
        this.customer_id = customer_id;
    }

    public MyNotif(){

    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotifTitle() {
        return notifTitle;
    }

    public void setNotifTitle(String notifTitle) {
        this.notifTitle = notifTitle;
    }

    public String getNotifBody() {
        return notifBody;
    }

    public void setNotifBody(String notifBody) {
        this.notifBody = notifBody;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getRecievedAT() {
        return recievedAT;
    }

    public void setRecievedAT(String recievedAT) {
        this.recievedAT = recievedAT;
    }



}
