package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ramdan Firdaus on 6/2/2017.
 */

public class PacketSubscr implements Parcelable {
    String name;
    String type;
    String abonemen;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAbonemen() {
        return abonemen;
    }

    public void setAbonemen(String abonemen) {
        this.abonemen = abonemen;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.abonemen);
    }

    public PacketSubscr() {
    }

    protected PacketSubscr(Parcel in) {
        this.name = in.readString();
        this.type = in.readString();
        this.abonemen = in.readString();
    }

    public static final Creator<PacketSubscr> CREATOR = new Creator<PacketSubscr>() {
        @Override
        public PacketSubscr createFromParcel(Parcel source) {
            return new PacketSubscr(source);
        }

        @Override
        public PacketSubscr[] newArray(int size) {
            return new PacketSubscr[size];
        }
    };
}
