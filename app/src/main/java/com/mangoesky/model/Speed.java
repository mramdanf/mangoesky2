/**
  * Generated by smali2java 1.0.0.558
  * Copyright (C) 2013 Hensence.com
  */

package com.mangoesky.model;


public class Speed {
    String datetime;
    String keterangan;
    int number;
    String speed;
    
    public Speed() {
    }
    
    public Speed(int number, String speed, String datetime, String keterangan) {
        this.number = number;
        this.speed = speed;
        this.datetime = datetime;
        this.keterangan = keterangan;
    }
    
    public void setSpeed(String speed) {
        this.speed = speed;
    }
    
    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
    
    public String getSpeed() {
        return speed;
    }
    
    public String getKeterangan() {
        return keterangan;
    }
    
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    
    public void setNumber(int number) {
        this.number = number;
    }
    
    public int getNumber() {
        return number;
    }
    
    public String getDatetime() {
        return datetime;
    }
}
