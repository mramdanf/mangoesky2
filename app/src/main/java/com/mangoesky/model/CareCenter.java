package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Ramdan Firdaus on 10/2/2017.
 */
/*
* {
      "name": "Care Center",
      "phone": "+6281212122016, +6281290002016 dan +6281289892016",
      "label": "Care Center : +6281212122016, +6281290002016 dan +6281289892016"
    },
* */
public class CareCenter implements Parcelable {

    List<String> email;

    List<String> phone;

    List<String> address;

    List<String> website;

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getWebsite() {
        return website;
    }

    public void setWebsite(List<String> website) {
        this.website = website;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.email);
        dest.writeStringList(this.phone);
        dest.writeStringList(this.address);
        dest.writeStringList(this.website);
    }

    public CareCenter() {
    }

    protected CareCenter(Parcel in) {
        this.email = in.createStringArrayList();
        this.phone = in.createStringArrayList();
        this.address = in.createStringArrayList();
        this.website = in.createStringArrayList();
    }

    public static final Creator<CareCenter> CREATOR = new Creator<CareCenter>() {
        @Override
        public CareCenter createFromParcel(Parcel source) {
            return new CareCenter(source);
        }

        @Override
        public CareCenter[] newArray(int size) {
            return new CareCenter[size];
        }
    };
}
