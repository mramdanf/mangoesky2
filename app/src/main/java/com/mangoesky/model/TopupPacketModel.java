package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 7/2/2017.
 */
/*{
        "id": "12",
        "name": "100 MB",
        "blanja_item_id": "13338644",
        "blanja_item_img_url": "https://s.blanja.com/picspace/469/100821/350.350_5ed9a8785af349b8b5dc9860dd26e770.jpg",
        "elevenia_item_id": "15260879",
        "price": "25000",
        "active_time_day": "30",
        "blanja_item_url": "http://item.blanja.com/item/13338644"
        },*/
public class TopupPacketModel implements Parcelable {
    String id;
    String name;
    @SerializedName("blanja_item_id")
    String blanjaitemId;
    @SerializedName("blanja_item_img_url")
    String blanjaItemImgUrl;
    @SerializedName("elevenia_item_id")
    String elevaniaItemId;
    String price;
    @SerializedName("active_time_day")
    String activeTimeDay;
    @SerializedName("blanja_item_url")
    String blanjaItemUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlanjaitemId() {
        return blanjaitemId;
    }

    public void setBlanjaitemId(String blanjaitemId) {
        this.blanjaitemId = blanjaitemId;
    }

    public String getBlanjaItemImgUrl() {
        return blanjaItemImgUrl;
    }

    public void setBlanjaItemImgUrl(String blanjaItemImgUrl) {
        this.blanjaItemImgUrl = blanjaItemImgUrl;
    }

    public String getElevaniaItemId() {
        return elevaniaItemId;
    }

    public void setElevaniaItemId(String elevaniaItemId) {
        this.elevaniaItemId = elevaniaItemId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActiveTimeDay() {
        return activeTimeDay;
    }

    public void setActiveTimeDay(String activeTimeDay) {
        this.activeTimeDay = activeTimeDay;
    }

    public String getBlanjaItemUrl() {
        return blanjaItemUrl;
    }

    public void setBlanjaItemUrl(String blanjaItemUrl) {
        this.blanjaItemUrl = blanjaItemUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.blanjaitemId);
        dest.writeString(this.blanjaItemImgUrl);
        dest.writeString(this.elevaniaItemId);
        dest.writeString(this.price);
        dest.writeString(this.activeTimeDay);
        dest.writeString(this.blanjaItemUrl);
    }

    public TopupPacketModel() {
    }

    protected TopupPacketModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.blanjaitemId = in.readString();
        this.blanjaItemImgUrl = in.readString();
        this.elevaniaItemId = in.readString();
        this.price = in.readString();
        this.activeTimeDay = in.readString();
        this.blanjaItemUrl = in.readString();
    }

    public static final Creator<TopupPacketModel> CREATOR = new Creator<TopupPacketModel>() {
        @Override
        public TopupPacketModel createFromParcel(Parcel source) {
            return new TopupPacketModel(source);
        }

        @Override
        public TopupPacketModel[] newArray(int size) {
            return new TopupPacketModel[size];
        }
    };
}
