package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ramdan Firdaus on 10/2/2017.
 */

public class CustomerService implements Parcelable {

    private List<String> email = null;
    private List<String> phone = null;
    private List<String> address = null;
    private List<String> website = null;

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getWebsite() {
        return website;
    }

    public void setWebsite(List<String> website) {
        this.website = website;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.email);
        dest.writeStringList(this.phone);
        dest.writeStringList(this.address);
        dest.writeStringList(this.website);
    }

    public CustomerService() {
    }

    protected CustomerService(Parcel in) {
        this.email = in.createStringArrayList();
        this.phone = in.createStringArrayList();
        this.address = in.createStringArrayList();
        this.website = in.createStringArrayList();
    }

    public static final Creator<CustomerService> CREATOR = new Creator<CustomerService>() {
        @Override
        public CustomerService createFromParcel(Parcel source) {
            return new CustomerService(source);
        }

        @Override
        public CustomerService[] newArray(int size) {
            return new CustomerService[size];
        }
    };
}
