package com.mangoesky.model;

/**
 * Created by user on 23/5/2016.
 */
public class HistoryTabModel {
    int numRowPacket;
    int numRowTopup;

    public HistoryTabModel(int numRowPacket, int numRowTopup, int numRowSpeedCh, String packetDescription,
                           String packetType, String packetAbodemen, String topupPacket, String topupTime,
                           String SCSpeed, String SCTime, String SCNotes) {
        this.numRowPacket = numRowPacket;
        this.numRowTopup = numRowTopup;
        this.numRowSpeedCh = numRowSpeedCh;
        this.packetDescription = packetDescription;
        this.packetType = packetType;
        this.packetAbodemen = packetAbodemen;
        this.topupPacket = topupPacket;
        this.topupTime = topupTime;
        this.SCSpeed = SCSpeed;
        this.SCTime = SCTime;
        this.SCNotes = SCNotes;
    }

    public HistoryTabModel() {
    }

    public String getSCNotes() {
        return SCNotes;
    }

    public void setSCNotes(String SCNotes) {
        this.SCNotes = SCNotes;
    }

    public int getNumRowPacket() {
        return numRowPacket;
    }

    public void setNumRowPacket(int numRowPacket) {
        this.numRowPacket = numRowPacket;
    }

    public int getNumRowTopup() {
        return numRowTopup;
    }

    public void setNumRowTopup(int numRowTopup) {
        this.numRowTopup = numRowTopup;
    }

    public int getNumRowSpeedCh() {
        return numRowSpeedCh;
    }

    public void setNumRowSpeedCh(int numRowSpeedCh) {
        this.numRowSpeedCh = numRowSpeedCh;
    }

    public String getPacketDescription() {
        return packetDescription;
    }

    public void setPacketDescription(String packetDescription) {
        this.packetDescription = packetDescription;
    }

    public String getPacketType() {
        return packetType;
    }

    public void setPacketType(String packetType) {
        this.packetType = packetType;
    }

    public String getPacketAbodemen() {
        return packetAbodemen;
    }

    public void setPacketAbodemen(String packetAbodemen) {
        this.packetAbodemen = packetAbodemen;
    }

    public String getTopupPacket() {
        return topupPacket;
    }

    public void setTopupPacket(String topupPacket) {
        this.topupPacket = topupPacket;
    }

    public String getTopupTime() {
        return topupTime;
    }

    public void setTopupTime(String topupTime) {
        this.topupTime = topupTime;
    }

    public String getSCSpeed() {
        return SCSpeed;
    }

    public void setSCSpeed(String SCSpeed) {
        this.SCSpeed = SCSpeed;
    }

    public String getSCTime() {
        return SCTime;
    }

    public void setSCTime(String SCTime) {
        this.SCTime = SCTime;
    }

    int numRowSpeedCh;
    String packetDescription, packetType, packetAbodemen;
    String topupPacket, topupTime;
    String SCSpeed, SCTime, SCNotes;

}
