package com.mangoesky.model;

/**
 * Created by Ramdan Firdaus on 9/2/2017.
 */

public class CoverEntity {
    int imageResId;

    public CoverEntity(int imageResId) {
        this.imageResId = imageResId;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }
}
