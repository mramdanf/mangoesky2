package com.mangoesky.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ramdan Firdaus on 6/2/2017.
 */

public class LogTopup implements Parcelable {
    @SerializedName("topup_amount")
    String topupAmount;
    @SerializedName("topup_datetime")
    String topupDatetime;

    public String getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(String topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getTopupDatetime() {
        return topupDatetime;
    }

    public void setTopupDatetime(String topupDatetime) {
        this.topupDatetime = topupDatetime;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.topupAmount);
        dest.writeString(this.topupDatetime);
    }

    public LogTopup() {
    }

    protected LogTopup(Parcel in) {
        this.topupAmount = in.readString();
        this.topupDatetime = in.readString();
    }

    public static final Creator<LogTopup> CREATOR = new Creator<LogTopup>() {
        @Override
        public LogTopup createFromParcel(Parcel source) {
            return new LogTopup(source);
        }

        @Override
        public LogTopup[] newArray(int size) {
            return new LogTopup[size];
        }
    };
}
