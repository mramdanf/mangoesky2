## API V2 ##
### Login ###
* URL: https://www.mangoesky.com/api_v2/apps/login
* Method: POST(reg_id)
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "msg": "Logged In",
  "user": {
    "id": "8",
    "email": "asrafin.danang@metrasat.co.id",
    "customer_id": "46000008",
    "fullname": "Asrafin Danang",
    "address": "Bogor",
    "gender": "M",
    "phone": "082113064268"
  }
}
```
### Profile ###
* URL: https://www.mangoesky.com/api_v2/apps/profile
* Method: GET(reg_id)
* Auth: Basic Auth
* Response
```json
{
   "success":true,
   "user":{
      "id":"8",
      "email":"asrafin.danang@metrasat.co.id",
      "customer_id":"310000000000",
      "fullname":"Asrafin Danang",
      "address":"Bogor",
      "gender":"M",
      "phone":"082113064268"
   }
}
```
### History ###
* URL: https://www.mangoesky.com/api_v2/apps/transaction_history?month=3&year=2016
* Methode: GET
* Auth: Basic Auth
* Response
```json
{
  "user": {
    "fullname": "Asrafin Danang",
    "address": "Jl Sholekh Iskandar, Bogor",
    "customer_id": "310000000000",
    "period": "Maret 2016"
  },
  "packet_subscr": [
    {
      "name": "Paket TRIAL Internet Broadband",
      "type": "Post Paid",
      "abonemen": "Rp 0"
    }
  ],
  "log_topup": [
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 17:07:27"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 16:57:54"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 16:40:12"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 16:18:18"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 15:41:15"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "29 March 2016 15:40:20"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "17 March 2016 15:49:09"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "17 March 2016 10:04:28"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "17 March 2016 09:21:37"
    },
    {
      "topup_amount": "300 MB",
      "topup_datetime": "16 March 2016 13:08:28"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 12:09:15"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "16 March 2016 12:03:39"
    },
    {
      "topup_amount": "100 MB",
      "topup_datetime": "16 March 2016 12:02:49"
    },
    {
      "topup_amount": "3 GB",
      "topup_datetime": "16 March 2016 11:57:13"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 11:47:44"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 11:25:10"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 11:18:02"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 10:46:19"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 10:12:06"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:35:41"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:33:39"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:22:21"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:15:42"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:15:21"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "16 March 2016 09:14:39"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "13 March 2016 17:29:00"
    },
    {
      "topup_amount": "500 MB",
      "topup_datetime": "13 March 2016 16:52:29"
    },
    {
      "topup_amount": "500 MB",
      "topup_datetime": "13 March 2016 16:50:45"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "09 March 2016 10:09:49"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "09 March 2016 10:07:54"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "09 March 2016 10:06:27"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "09 March 2016 10:04:54"
    },
    {
      "topup_amount": "10 MB",
      "topup_datetime": "09 March 2016 10:03:16"
    }
  ],
  "log_speed_change": [
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "09 March 2016 10:03:14",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "09 March 2016 10:04:53",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "09 March 2016 10:06:26",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "09 March 2016 10:07:53",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "09 March 2016 10:09:48",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "13 March 2016 16:50:44",
      "desc": "Topup 500 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "13 March 2016 16:52:28",
      "desc": "Topup 500 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "13 March 2016 17:29:00",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:14:37",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:15:20",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:15:42",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:22:21",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:33:38",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 09:35:40",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 10:12:05",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 10:46:18",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 11:18:01",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 11:25:09",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 11:47:43",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 11:57:12",
      "desc": "Topup 3 GB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 12:02:48",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 12:03:38",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "2 Mbps",
      "upload_speed": "512 Kbps",
      "datetime_change": "16 March 2016 12:04:02",
      "desc": "Alert sisa kuota 200 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 12:09:13",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "128 Kbps",
      "upload_speed": "32 Kbps",
      "datetime_change": "16 March 2016 12:11:02",
      "desc": "Kuota habis"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "16 March 2016 13:08:27",
      "desc": "Topup 300 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "17 March 2016 09:21:36",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "17 March 2016 10:04:27",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "17 March 2016 15:49:07",
      "desc": "Topup 10 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 15:40:19",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 15:41:14",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 16:18:17",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 16:40:11",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 16:57:53",
      "desc": "Topup 100 MB"
    },
    {
      "download_speed": "0 bps",
      "upload_speed": "0 bps",
      "datetime_change": "29 March 2016 17:07:26",
      "desc": "Topup 100 MB"
    }
  ]
}
```
### List Topup (Topup Menu Page) ###
* URL: https://www.mangoesky.com/api_v2/apps/topup_packet
* Methode: GET
* Auth: Basic Auth
* Response
```json
{
    "success": true,
    "topup_packet": [
          {
            "id": "12",
            "name": "100 MB",
            "blanja_item_id": "13338644",
            "blanja_item_img_url": "https://s.blanja.com/picspace/469/100821/350.350_5ed9a8785af349b8b5dc9860dd26e770.jpg",
            "elevenia_item_id": "15260879",
            "price": "25000",
            "active_time_day": "30",
            "blanja_item_url": "http://item.blanja.com/item/13338644"
        },
        {
            "id": "14",
            "name": "300 MB",
            "blanja_item_id": "13338645",
            "blanja_item_img_url": "https://s.blanja.com/picspace/469/100821/350.350_f217d46e4cf144c399d1048c67deb81a.jpg",
            "elevenia_item_id": "15260593",
            "price": "50000",
            "active_time_day": "30",
            "blanja_item_url": "http://item.blanja.com/item/13338645"
        },
```
### Edit Profile ###
* URL: https://www.mangoesky.com/api_v2/apps/update_profile
* Methode: POST (fullname, gender, email, phone, address)
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "msg": "Perubahan profil berhasil."
}
```
### Change Password ###
* URL: https://www.mangoesky.com/api_v2/apps/change_password
* Methode: POST (old_password, new_password, confirm_new_password)
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "msg": "Password Anda berhasil diganti."
}
```
### Change Password ###
* URL: https://www.mangoesky.com/api_v2/apps/logout
* Methode: POST (reg_id)
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "msg": "Logged Out"
}
```
### Dashboard ###
* URL: https://www.mangoesky.com/api_v2/apps/dashboard
* Methode: GET
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "fullname": "Asrafin Danang",
  "customer_id": "310000000000",
  "member_packet": "Paket TRIAL Internet Broadband",
  "service_stat": {
    "success": true,
    "customer_name": "Asrafin Danang",
    "active_time_exceeded": false,
    "quota_exceeded": false,
    "datetime_end": 1488527202,
    "download_speed": "2 Mbps",
    "upload_speed": "512 Kbps",
    "current_speed": "up to 2 Mbps / 512 Kbps",
    "throttle_level": "0",
    "date_end": "3 Mar 2017",
    "time_end": "14:46:42",
    "remaining_quota": 2490107570,
    "remaining_quota_fmt": "2.49 GB"
  }
}
```
### Customer Service ###
* URL: https://www.mangoesky.com/api_v2/apps/customer_service
* Methode: GET
* Auth: Basic Auth
* Response
```json
{
  "success": true,
  "customer_service": [
    {
      "name": "Care Center",
      "phone": "+6281212122016, +6281290002016 dan +6281289892016",
      "label": "Care Center : +6281212122016, +6281290002016 dan +6281289892016"
    },
    {
      "name": "Email",
      "phone": "cs.broadband@metrasat.co.id",
      "label": "Email : cs.broadband@metrasat.co.id"
    },
    {
      "name": "Website",
      "phone": "https:\/\/www.mangoesky.com\nhttp:\/\/metrasatstore.blanja.com\/",
      "label": "Website : https:\/\/www.mangoesky.com\nhttp:\/\/metrasatstore.blanja.com\/"
    },
    {
      "name": "Playstore",
      "phone": "https:\/\/play.google.com\/store\/apps\/details?id=com.mangoesky.app",
      "label": "Playstore : https:\/\/play.google.com\/store\/apps\/details?id=com.mangoesky.app"
    },
    {
      "name": "Appstore",
      "phone": "https:\/\/itunes.apple.com\/us\/app\/mangoesky\/id1071050619?ls=1&mt=8",
      "label": "Appstore : https:\/\/itunes.apple.com\/us\/app\/mangoesky\/id1071050619?ls=1&mt=8"
    }
  ]
}

```
### Customer Service ###
* URL: https://www.mangoesky.com/api_v2/apps/customer_service_dev
* Methode: GET
* Auth: Basic Auth
* Response
```json
{
  "customer_service": {
    "email": [
      "noc@metrasat.co.id",
      "noc@metrasat.com",
      "cs.broadband@metrasat.co.id"
    ],
    "phone": [
      "+6281212122016",
      "+6281290002016",
      "+6281289892016"
    ],
    "address": [
      "Jl. KH Soleh Iskandar KM 6, Tanah Sareal, Bogor"
    ],
    "website": [
      "www.metrasat.co.id",
      "www.mangoesky.com"
    ]
  }
}
```
### Submit Voucher ###
* URL: https://www.mangoesky.com/api_v2/apps/submit_topup
* Methode: POST (voucher_code)
* Auth: Basic Auth
* Response
```json
{
  "success": false,
  "msg": "Kode voucher yang Anda masukkan tidak terdaftar."
}
```
### Pay Topup ### 
* URL: https://www.mangoesky.com/api_v2/apps/topup_finpay
* Method: POST(topup_packet_id)
* Auth: Basic Auth
* Response
```json
{
    "success": true,
    "msg": "",
    "topup_order": {
        "invoice": "TOPMSKYFP1480394012",
        "type": "finpay",
        "code": "021113127978",
        "timeout": "60",
        "price": "25000",
        "puchase_date": "2016-11-29 11:33:34",
        "topup_packet_id": "12",
        "topup_packet_name": "100 MB",
        "expiration_date": "2016-11-29 12:33:34"
    }
}
```
### Topup History ### 
* URL: https://www.mangoesky.com/api_v2/apps/topup_order
* Method: GET
* Auth: Basic Auth
* Json Response
```json
{
        "success":true,
        "msg":"",
        "topup_orders":[
            {
                "id":"97",
                "invoice":"TOPMSKYFP1480394012",
                "type":"finpay",
                "price":"25000",
                "status":"unpaid",
                "topup_packet_name":"100 MB"
            },
            .
            .
            .
         ]

```
### Topup Order Detail ### 
* URL: https://www.mangoesky.com/api_v2/apps/{order_id}
* Method: GET(order_id) - direct in URL
* Response Wrapper, Enpoint: DetailTHResponse, HistoryToupEndPoint
* Json Response
```json
{
    "success": true,
    "msg": "",
    "topup_order": {
        "invoice": "TOPMSKYFP1480394012",
        "type": "finpay",
        "code": "021113127978",
        "timeout": "60",
        "price": "25000",
        "puchase_date": "2016-11-29 11:33:34",
        "topup_packet_id": "12",
        "topup_packet_name": "100 MB",
        "expiration_date": "2016-11-29 12:33:34"
    }
}
```